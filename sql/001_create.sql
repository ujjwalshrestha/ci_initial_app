-- auth_user
-- Table for maintaining data related to user session/authentication
-- By Ujjwal Shrestha (2020-12-21)
CREATE TABLE auth_user (
    id int PRIMARY KEY auto_increment,
    ip_address varchar(50),
    username varchar(50) NOT NULL,
    password varchar(255) NOT NULL,
    email varchar(100) NOT NULL,
    activation_code varchar(100) DEFAULT NULL,
    forgot_password_code varchar(100) DEFAULT NULL,
    forgot_password_datetime timestamp NULL DEFAULT NULL,
    temp_status tinyint DEFAULT 1,
    active_status tinyint DEFAULT 1,
    last_login timestamp NULL DEFAULT NULL,
    is_super_admin int(2) DEFAULT 0,
    created_by int NOT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by int DEFAULT NULL,
    updated_at timestamp NULL DEFAULT NULL
) CHARSET=utf8 DEFAULT COLLATE utf8_general_ci engine=innodb;


-- user_detail
-- Table for maintaining details of users
-- By Ujjwal Shrestha (2020-12-21)
CREATE TABLE user_detail (
    id int PRIMARY KEY auto_increment,
    user_id int NOT NULL,
    FOREIGN KEY (user_id) REFERENCES auth_user(id)
    ON UPDATE CASCADE ON DELETE CASCADE,
    firstname varchar(100),
    lastname varchar(100),
    dob date DEFAULT NULL,
    address varchar(200) DEFAULT NULL,
    gender varchar(2) NOT NULL,
    contact varchar(20),
    profile_image varchar(255) DEFAULT NULL,
    created_by int NOT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by int DEFAULT NULL,
    updated_at timestamp NULL DEFAULT NULL  
) CHARSET=utf8 DEFAULT COLLATE utf8_general_ci engine=innodb;


-- auth_group
-- Table for maintaining user's groups
-- By Ujjwal Shrestha (2020-12-21)
CREATE TABLE auth_group (
    id int PRIMARY KEY auto_increment,
    group_name varchar(100),
    description text DEFAULT NULL, 
    login_status int(2) DEFAULT 1,
    status int(2) DEFAULT 1
) CHARSET=utf8 DEFAULT COLLATE utf8_general_ci engine=innodb;


-- auth_user_group_mapping
-- Table for mapping users and its respective group.
-- By Ujjwal Shrestha (2020-12-21)
CREATE TABLE auth_user_group_mapping (
    id int PRIMARY KEY auto_increment,
    user_id int NOT NULL,
    FOREIGN KEY (user_id) REFERENCES auth_user(id)
    ON UPDATE CASCADE ON DELETE CASCADE,
    group_id int NOT NULL,
    FOREIGN KEY (group_id) REFERENCES auth_group(id)
    ON UPDATE CASCADE ON DELETE CASCADE,
    created_by int NOT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by int DEFAULT NULL,
    updated_at timestamp NULL DEFAULT NULL
) CHARSET=utf8 DEFAULT COLLATE utf8_general_ci engine=innodb;


-- dashboard_menu
-- Table for maintaining menu of dashboard
-- By Ujjwal Shrestha (2020-12-21)
CREATE TABLE dashboard_menu (
    id int PRIMARY KEY auto_increment,
    menu_type varchar(2) NOT NULL,
    parent_menu_id int DEFAULT NULL,
    FOREIGN KEY (parent_menu_id) REFERENCES dashboard_menu(id)
    ON UPDATE CASCADE ON DELETE CASCADE,
    menu_name varchar(100) NOT NULL,
    slug varchar(150) NOT NULL,
    menu_uri varchar(100) DEFAULT '#',
    menu_icon varchar(30) NOT NULL,
    order_by int(5),
    status int(2) DEFAULT 1,
    created_by int NOT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by int DEFAULT NULL,
    updated_at timestamp NULL DEFAULT NULL
) CHARSET=utf8 DEFAULT COLLATE utf8_general_ci engine=innodb;


-- dashboard_menu_access
-- Table for determining access level of dashboard menu
-- By Ujjwal Shrestha (2020-12-21)
CREATE TABLE dashboard_menu_access (
    id int PRIMARY KEY auto_increment,
    group_id int NOT NULL,
    FOREIGN KEY (group_id) REFERENCES auth_group(id)
    ON DELETE CASCADE,
    menu_id int NOT NULL,
    FOREIGN KEY (menu_id) REFERENCES dashboard_menu(id)
    ON DELETE CASCADE,
    view_access tinyint DEFAULT NULL,
    add_access tinyint DEFAULT NULL,
    edit_access tinyint DEFAULT NULL,
    delete_access tinyint DEFAULT NULL,
    created_by int NOT NULL,
    created_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    updated_by int DEFAULT NULL,
    updated_at timestamp NULL DEFAULT NULL
) CHARSET=utf8 DEFAULT COLLATE utf8_general_ci engine=innodb;


-- user_subscription
-- Table for user subscription email
-- By Ujjwal Shrestha (2021-01-20)
CREATE TABLE user_subscription (
    id int PRIMARY KEY auto_increment,
    email varchar(150) NOT NULL,
    hash_code varchar(255) NOT NULL,
    status tinyint DEFAULT 1,
    subscribed_at timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL,
    unsubscribed_at timestamp NULL DEFAULT NULL
) CHARSET=utf8 DEFAULT COLLATE utf8_general_ci engine=innodb;