-- tbl_name
-- Description
-- By Author_name (Date)


-- tbl_users_main
-- To add a column named LAST_LOGIN after ACTIVE_STATUS
-- By Ujjwal Shrestha (2019-07-27)
ALTER TABLE tbl_users_main ADD LAST_LOGIN TIMESTAMP DEFAULT NULL AFTER ACTIVE_STATUS;


-- tbl_user_groups
-- To add a column named LOGIN_STATUS, STATUS after DESCRIPTION, LOGIN_STATUS
-- By Ujjwal Shrestha (2019-09-29)
ALTER TABLE tbl_user_groups ADD LOGIN_STATUS INT(2) DEFAULT 1 AFTER DESCRIPTION;
ALTER TABLE tbl_user_groups ADD STATUS INT(2) DEFAULT 1 AFTER LOGIN_STATUS;


-- tbl_users_main
-- To add a column named IS_SUPER_ADMIN after LAST_LOGIN
-- By Ujjwal Shrestha (2019-09-29)
ALTER TABLE tbl_users_main ADD IS_SUPER_ADMIN INT(2) DEFAULT 0 AFTER LAST_LOGIN;


-- tbl_users_detail
-- To add a column named GENDER after DOB
-- By Ujjwal Shrestha (2019-09-29)
ALTER TABLE tbl_users_detail ADD GENDER VARCHAR(2) DEFAULT NOT NULL AFTER DOB;


-- tbl_user_group_mapping
-- To add a column named GENDER after DOB
-- By Ujjwal Shrestha (2019-09-29)
ALTER TABLE tbl_user_group_mapping ADD CREATED_BY INT NOT NULL AFTER GROUP_ID;
ALTER TABLE tbl_user_group_mapping ADD CREATED_AT TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL AFTER CREATED_BY;
ALTER TABLE tbl_user_group_mapping ADD UPDATED_BY INT NULL AFTER CREATED_AT;
ALTER TABLE tbl_user_group_mapping ADD UPDATED_AT TIMESTAMP NULL AFTER UPDATED_BY;


ALTER TABLE user_subscription ADD hash_code varchar(255) NOT NULL AFTER email;

ALTER TABLE user_detail ADD contact varchar(20) DEFAULT NULL AFTER gender;

ALTER TABLE auth_group ADD profile_update_access int(2) DEFAULT 1 AFTER description;