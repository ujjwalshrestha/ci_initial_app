-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 06, 2021 at 08:35 AM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ci_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `group_name` varchar(100) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `profile_update_access` int(2) DEFAULT 1,
  `login_status` int(2) DEFAULT 1,
  `status` int(2) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_group`
--

INSERT INTO `auth_group` (`id`, `group_name`, `description`, `profile_update_access`, `login_status`, `status`) VALUES
(1, 'admin', 'Administration', 1, 1, 1),
(2, 'editor', 'Editor', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `activation_code` varchar(100) DEFAULT NULL,
  `forgot_password_code` varchar(100) DEFAULT NULL,
  `forgot_password_datetime` timestamp NULL DEFAULT NULL,
  `temp_status` tinyint(4) DEFAULT 1,
  `active_status` tinyint(4) DEFAULT 1,
  `last_login` timestamp NULL DEFAULT NULL,
  `is_super_admin` int(2) DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_user`
--

INSERT INTO `auth_user` (`id`, `ip_address`, `username`, `password`, `email`, `activation_code`, `forgot_password_code`, `forgot_password_datetime`, `temp_status`, `active_status`, `last_login`, `is_super_admin`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, '::1', 'superadmin', 'bb0113ad2b43d8a478d1c53f0f4128cbea42344eb303e028ccd591da4e81ee533c97a99d9e2b0b6bc0577af9260734f40ac9de9b1bab294fdd4611d3edb1e069xGqw4CmKWKFZakRIWlzFs1VoYqQA8LGpR2++JBFF6xyG8LdS4oM7H1JMcUyNRook', 'admin@gmail.com', NULL, NULL, NULL, 1, 1, '2021-02-06 02:43:43', 1, 1, '2019-07-24 04:08:02', 1, '2021-02-06 02:44:58'),
(5, '::1', 'ujjwal', 'b45bb25df8fd2de7f19d9e69c1853a9163b4db7065b50d85a78ff75550fbabd7c34f8d2687d8cdefa23b253752f957df710d861d4b9726974d542ef665d88ab9sAdVfAeuN7SgMaVxcSUtsnRc6kodhF+rZA8oOB0sT2n7Xdwt0Jmi7+Kea+LlSPYe', 'ujjwalshrestha1996@gmail.com', NULL, NULL, NULL, 1, 1, '2021-02-06 02:46:09', 0, 1, '2020-12-22 06:18:07', 1, '2021-02-06 02:49:50');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_group_mapping`
--

CREATE TABLE `auth_user_group_mapping` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auth_user_group_mapping`
--

INSERT INTO `auth_user_group_mapping` (`id`, `user_id`, `group_id`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 1, 1, 1, '2019-10-17 12:01:38', NULL, NULL),
(4, 5, 2, 1, '2020-12-22 06:18:07', 1, '2021-02-06 02:49:50');

-- --------------------------------------------------------

--
-- Table structure for table `dashboard_menu`
--

CREATE TABLE `dashboard_menu` (
  `id` int(11) NOT NULL,
  `menu_type` varchar(2) NOT NULL,
  `parent_menu_id` int(11) DEFAULT NULL,
  `menu_name` varchar(100) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `menu_uri` varchar(100) DEFAULT '#',
  `menu_icon` varchar(30) NOT NULL,
  `order_by` int(5) DEFAULT NULL,
  `status` int(2) DEFAULT 1,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dashboard_menu`
--

INSERT INTO `dashboard_menu` (`id`, `menu_type`, `parent_menu_id`, `menu_name`, `slug`, `menu_uri`, `menu_icon`, `order_by`, `status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 's', NULL, 'Dashboard', 'dashboard', '#', 'fas fa-tachometer-alt', 1, 1, 1, '2019-09-06 06:31:06', 1, '2021-01-25 16:59:56'),
(2, 'p', NULL, 'User Management', 'user-management', '#', 'fas fa-users', 2, 1, 1, '2019-09-06 08:40:20', NULL, NULL),
(3, 'c', 2, 'Manage User Group', 'manage-user-group', 'admin-session/manage-user-group', 'far fa-circle', 1, 1, 1, '2019-09-06 10:42:15', 1, '2021-01-28 00:54:30'),
(4, 'p', NULL, 'Dashboard Menu', 'dashboard-menu', '#', 'fas fa-bars', 3, 1, 1, '2019-09-07 21:43:46', 1, '2021-01-28 00:56:21'),
(5, 'c', 4, 'Manage Menu Access', 'manage-menu-access', '#', 'far fa-circle', 2, 1, 1, '2019-09-07 21:45:05', 1, '2021-01-28 00:57:00'),
(6, 'c', 2, 'Manage Users', 'manage-users', 'admin-session/manage-users', 'far fa-circle', 2, 1, 1, '2019-09-24 03:44:10', 1, '2021-01-28 00:54:17'),
(7, 'c', 4, 'Manage Menu', 'manage-menu', 'admin-session/manage-menu', 'far fa-circle', 1, 1, 1, '2019-09-30 04:20:58', 1, '2021-01-28 00:56:36');

-- --------------------------------------------------------

--
-- Table structure for table `dashboard_menu_access`
--

CREATE TABLE `dashboard_menu_access` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `view_access` tinyint(4) DEFAULT NULL,
  `add_access` tinyint(4) DEFAULT NULL,
  `edit_access` tinyint(4) DEFAULT NULL,
  `delete_access` tinyint(4) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `dashboard_menu_access`
--

INSERT INTO `dashboard_menu_access` (`id`, `group_id`, `menu_id`, `view_access`, `add_access`, `edit_access`, `delete_access`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 1, 1, 0, 0, 0, 0, 1, '2021-01-29 15:05:27', 1, '2021-02-02 17:08:27'),
(2, 1, 4, 0, 0, 0, 0, 1, '2021-01-29 15:05:27', 1, '2021-02-02 17:08:27'),
(3, 1, 7, 0, 0, 0, 0, 1, '2021-01-29 15:05:28', 1, '2021-02-02 17:08:27'),
(4, 1, 5, 0, 0, 0, 0, 1, '2021-01-29 15:05:28', 1, '2021-02-02 17:08:27'),
(5, 1, 3, 1, 0, 0, 0, 1, '2021-01-29 15:05:28', 1, '2021-02-02 17:08:27'),
(6, 1, 6, 1, 1, 1, 1, 1, '2021-01-29 15:05:28', 1, '2021-02-02 17:08:27'),
(7, 1, 2, 1, 0, 0, 0, 1, '2021-01-29 15:05:28', 1, '2021-02-02 17:08:27'),
(8, 2, 1, 0, 0, 0, 0, 1, '2021-02-05 17:29:35', NULL, NULL),
(9, 2, 4, 0, 0, 0, 0, 1, '2021-02-05 17:29:35', NULL, NULL),
(10, 2, 7, 0, 0, 0, 0, 1, '2021-02-05 17:29:35', NULL, NULL),
(11, 2, 5, 0, 0, 0, 0, 1, '2021-02-05 17:29:35', NULL, NULL),
(12, 2, 3, 1, 1, 1, 1, 1, '2021-02-05 17:29:35', NULL, NULL),
(13, 2, 6, 1, 1, 1, 1, 1, '2021-02-05 17:29:35', NULL, NULL),
(14, 2, 2, 1, 0, 0, 0, 1, '2021-02-05 17:29:35', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_detail`
--

CREATE TABLE `user_detail` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `firstname` varchar(100) DEFAULT NULL,
  `lastname` varchar(100) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `gender` varchar(2) NOT NULL,
  `contact` varchar(20) DEFAULT NULL,
  `profile_image` varchar(255) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_detail`
--

INSERT INTO `user_detail` (`id`, `user_id`, `firstname`, `lastname`, `dob`, `address`, `gender`, `contact`, `profile_image`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 1, 'Admin', 'istrator', NULL, '', 'm', '', NULL, 1, '2019-07-24 04:14:35', 1, '2021-02-06 02:44:58'),
(4, 5, 'Ujjwal', 'Shrestha', '1996-04-17', 'Baluwatar - 4, Nayabasti Marg', 'm', '9841739097', NULL, 1, '2020-12-22 06:18:07', 1, '2021-02-06 02:49:50');

-- --------------------------------------------------------

--
-- Table structure for table `user_subscription`
--

CREATE TABLE `user_subscription` (
  `id` int(11) NOT NULL,
  `email` varchar(150) NOT NULL,
  `token` varchar(255) NOT NULL,
  `status` tinyint(4) DEFAULT 1,
  `subscribed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `unsubscribed_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_user_group_mapping`
--
ALTER TABLE `auth_user_group_mapping`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `dashboard_menu`
--
ALTER TABLE `dashboard_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_menu_id` (`parent_menu_id`);

--
-- Indexes for table `dashboard_menu_access`
--
ALTER TABLE `dashboard_menu_access`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `menu_id` (`menu_id`);

--
-- Indexes for table `user_detail`
--
ALTER TABLE `user_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `user_subscription`
--
ALTER TABLE `user_subscription`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `auth_user_group_mapping`
--
ALTER TABLE `auth_user_group_mapping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `dashboard_menu`
--
ALTER TABLE `dashboard_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `dashboard_menu_access`
--
ALTER TABLE `dashboard_menu_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `user_detail`
--
ALTER TABLE `user_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_subscription`
--
ALTER TABLE `user_subscription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_user_group_mapping`
--
ALTER TABLE `auth_user_group_mapping`
  ADD CONSTRAINT `auth_user_group_mapping_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_user_group_mapping_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `dashboard_menu`
--
ALTER TABLE `dashboard_menu`
  ADD CONSTRAINT `dashboard_menu_ibfk_1` FOREIGN KEY (`parent_menu_id`) REFERENCES `dashboard_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `dashboard_menu_access`
--
ALTER TABLE `dashboard_menu_access`
  ADD CONSTRAINT `dashboard_menu_access_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `dashboard_menu_access_ibfk_2` FOREIGN KEY (`menu_id`) REFERENCES `dashboard_menu` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_detail`
--
ALTER TABLE `user_detail`
  ADD CONSTRAINT `user_detail_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
