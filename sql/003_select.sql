-- tbl_dash_menu
-- Select query for dashboard menu
-- By Ujjwal Shrestha (2019-09-06)
SELECT * 
FROM tbl_dash_menu AS P 
LEFT JOIN tbl_dash_menu AS C 
ON P.ID = C.PARENT_MENU_ID
WHERE P.STATUS = 1 OR C.STATUS = 1