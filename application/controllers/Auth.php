<?php 

if( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * This controller is for user authentication purpose.
 */
class Auth extends MX_Controller
{
    /*
	 *	variable to hold data for view
	 */
	private static $viewData = array();
	
	public function __construct()
	{
		parent::__construct();
		$this->form_validation->CI =& $this;

		// To show the username in admin dashboard of the logged in user
		if (uri_segment(2) != 'login') {
			self::$viewData['username'] = $this->auth_model->getAuthUser()->username;
		}

		// Dashboard side menu
		self::$viewData['dashboardSideMenu'] = $this->admin_model->getDashboardSideMenu();

		// Checks if the auth active status, auth temp_status and user group login_status is 0 or not. If one of them is 0, then user is forced to logout of the system.
		if ( (uri_segment(2) != 'login') && (($this->auth_model->getAuthUser()->active_status == 0) || ($this->auth_model->getAuthUser()->temp_status == 0) || ($this->auth_model->getUserGroupById(session_data('user_id'))->login_status == 0)) ) 
		{
			$this->logout(); // force logout
		}
	}

	public function index()
	{
		show_404();
	}	

	/**
	 * Through this function, password is updated
	 *
	 * @param string $param The parameters passed will be checked according to which certain functionalities are carried out.
	 * @return void
	 */
	public function changePassword($param)
	{
		if ($this->input->post()) {

			// If the parameter passed is 'admin'.
			if ($param == 'admin') {
				self::$viewData['title'] = "Change Password";
				self::$viewData['breadcrumb'] = "Change Password"; 
				self::$viewData['page'] = "change_password";
				$templ = TEMPL_ADMIN;
				$redirect_url = 'admin-session/change-password';
			}

			$this->form_validation->set_rules('oldpassword', ' ', 'required|callback_oldpassword_check');
			$this->form_validation->set_rules('newpassword', ' ', 'required');
			$this->form_validation->set_rules('confirmpassword', ' ', 'required|matches[newpassword]',
				array('matches' => 'The confirm password field does not match the new password field.')
			);
			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view($templ, self::$viewData);

			} else {
				$newpassword = $this->input->post('newpassword');
				$password_encrypted = encrypt_password($newpassword); // encrypt password
				$updatePassword = $this->auth_model->updatePassword($password_encrypted);

				if ($updatePassword) {
					$message = 'Password has been updated.';
					$this->session->set_flashdata('success_msg', $message);
					redirect($redirect_url, 'refresh'); 

				} else {
					$message = 'Password update failed.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirect_url, 'refresh'); 
				}
			}
		}
	}


	/**
	 * Through this function, the old password is checked if it matches with the one stored in the database or not.
	 *
	 * @param string $oldpassword Old password fetched from user input.
	 * @return void
	 */
	public function oldpassword_check($oldpassword)
	{
		// If oldpassword is empty
		if (!$oldpassword) {
			$this->form_validation->set_message('oldpassword_check', 'The field is required.');
		   	return FALSE;
		}

		$getUserSessionDetail = $this->auth_model->getAuthUser();
		$oldpassword_db = decrypt_password($getUserSessionDetail->password);
	 
		// If old password does not match
		if ($oldpassword != $oldpassword_db) {
		   $this->form_validation->set_message('oldpassword_check', 'The old password did not match.');
		   return FALSE;
		   
		} else {
			return TRUE;
		}

		
	}

    // public function do_login()
    // {
    //     echo uri_segment(1);exit;
    //     if ($this->input->post()) {
	// 		// Form-validation
	// 		$this->form_validation->set_rules('username', ' ', 'required');
	// 		$this->form_validation->set_rules('password', ' ', 'required');
	// 		$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

	// 		if (!$this->form_validation->run()) {
	// 			// If validation is false, then return to page login and show validation error. 
	// 			$this->load->view('admin-session/login');

	// 		} else {
	// 			$username = $this->input->post('username');
	// 			$password = $this->input->post('password');

	// 			$check_login = $this->auth_model->check_login($username, $password);

	// 			if ($check_login == false) {
	// 				$message = 'Username or password incorrect.';
	// 				$this->session->set_flashdata('error_msg', $message);
	// 				redirect('admin-session/login', 'refresh'); 

	// 			} else {
	// 				if ($check_login == '403') {
	// 					$message = 'Your account is not active.';
	// 					$this->session->set_flashdata('error_msg', $message);
	// 					redirect('admin-session/login', 'refresh'); 

	// 				} elseif ($check_login == 'success') {
	// 					redirect('admin-session/dashboard', 'refresh'); 
	// 				}
	// 			}
	// 		}

	// 	}
    // }
    
}