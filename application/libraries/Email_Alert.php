<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Libarary for Email Alert
 */
class Email_Alert
{
	/**
	 * 	Codeigniter instance
	 * @var instance
	 */
	private $CI;
	function __construct()
	{
		$this->CI = & get_instance();
	}

	/**
	 * Funcation that send email alert
	 * @return 
	 */
	public function sendMail($to, $from, $from_title = null, $subject, $message, $cc = array(), $attachment = array())
	{
		try {
            // Setting up email configuration
			$config = Array(
                'protocol' 	=> 'smtp',
                'smtp_host' => 'ssl://smtp.gmail.com',
                'smtp_port' =>	465,
                'smtp_user' => 'noreply.micheck@gmail.com',
                'smtp_pass' => 'rldvyjrclcqczpta',
                'mailtype'  => 'html', 
                'charset'   => 'iso-8859-1'
            );
            
            // Checks email configuration
			if (!$config) {
				throw new Exception("Invalid email configuration", 1);
            }
            
            // Checks sender's email address
			if (empty($from)) {
				throw new Exception("Please specify sender's email address.", 1);
			}

			// Checks recipient's email address
			if (empty($to)) {
				throw new Exception("Please specify recipient's email address.", 1);
			}

			// Checks email subject
			if (empty($subject)) {
				throw new Exception("Please specify email subject.", 1);
			}

			// Checks email message
			if (empty($message)) {
				throw new Exception("Please specify email message.", 1);
            }
            
            // Initialize email configuration
            $this->CI->email->initialize($config);
            $this->CI->email->set_newline("\r\n");

            // sets from (sender's email). Here, from_title is the name of sender (eg: organization's name) 
            $this->CI->email->from($from, $from_title);

            // checks if the email of recipient is more than one or not.
            if(is_array($to) && count($to) > 0){
	        	foreach ($to as $t) {
	        		$this->CI->email->to($t);
	        	}
	        } else {
	        	$this->CI->email->to($to);
            }

            // sets email subject
            $this->CI->email->subject($subject);

            // sets email message
            $this->CI->email->message($message);

            // checks if the email of cc is more than one or not.
            if(is_array($cc) && count($cc) > 0){
	        	foreach ($cc as $c) {
	        		$this->CI->email->cc($c);
	        	}
	        } else {
	        	$this->CI->email->cc($cc);
            }

            // checks if there is attachment or not
            if ($attachment) {
                $this->CI->email->attach($attachment);
            }

            // send email
            $this->CI->email->send();

	        // echo $this->CI->email->print_debugger();

		} catch (Exception $e) {
			throw new Exception("Error Processing Request", 1);
		}
		
	}
}