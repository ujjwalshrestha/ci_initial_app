<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller'] = 'site/index'; 
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// routing for admin section
$route['admin'] = 'admin';

$route['admin-session/user-profile'] = 'admin/userProfile';
$route['admin-session/user-profile-update'] = 'admin/userProfileUpdate';
$route['admin-session/change-password'] = 'admin/passwordSettingView';
$route['admin-session/change-password/(:any)'] = 'auth/changePassword/$1';


// Dashboard Menu
$route['admin-session/manage-menu'] = 'admin/manage_menu';
$route['admin-session/dashboard-menu-add'] = 'admin/dashboard_menu_add';
$route['admin-session/dashboard-menu-edit/(:any)'] = 'admin/dashboard_menu_edit/$1';

// Manage Users
$route['admin-session/manage-users'] = 'admin/manage_users';
$route['admin-session/user-add'] = 'admin/user_add';
$route['admin-session/user-edit/(:any)'] = 'admin/user_edit/$1';
$route['admin-session/manage-menu-access'] = 'admin/manage_menu_access';

// Manage User Group
$route['admin-session/manage-user-group'] = 'admin/manage_user_group';
$route['admin-session/user-group-add'] = 'admin/user_group_add';
$route['admin-session/user-group-edit/(:any)'] = 'admin/user_group_edit/$1';

$route['admin-session/(:any)'] = 'admin/$1';
$route['admin-session/(:any)/(:any)'] = 'admin/$1/$2';
$route['auth/(:any)'] = 'auth/$1';


// routing for coming soon
$route['coming-soon'] = 'site/comingSoon';

// routing for site section
$route['(:any)'] = 'site/$1';
