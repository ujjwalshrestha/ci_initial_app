<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}
	
	public function getAuthUserDetail()
	{
		$user_id = session_data('user_id'); // userid from session

		$where = array(
			'a.id' => $user_id
		);

		$result = $this->db->select('d.firstname, d.lastname, d.dob, d.address, d.gender, d.contact, a.username, a.email')
						   ->from('auth_user as a')
						   ->join('user_detail as d', 'a.id = d.user_id')
						   ->where($where)
						   ->get();

		if ($result->num_rows() == 1) {
			return $result->row();

		} else {
			return false;
		}
	}

	public function getDashboardSideMenu()
	{
		$where = array(
			'status' => 1,
			'parent_menu_id' => NULL
		);

		$this->db->order_by('order_by');
		$result = $this->db->get_where('dashboard_menu', $where);

		if ($result->num_rows() > 0) {
			return $result->result();

		} else {
			return false;
		}
	}

	public function getParentMenu()
	{
		$where = array(
			'status' => 1,
			'menu_type' => 'p'
		);

		$result = $this->db->get_where('dashboard_menu', $where);

		if ($result->num_rows() > 0) {
			return $result->result();

		} else {
			return false;
		}
	}

	public function insertDashboardMenu($insert_data)
	{
		return $this->db->insert('dashboard_menu', $insert_data);
	}

	public function getDashboardMenuList()
	{
		// $where = array(
		// 	'DM1.STATUS' => 1
		// );

		$result = $this->db->select('dm1.id, dm1.menu_name, dm2.menu_name as parent_menu_name, dm1.menu_uri, dm1.status, au.username, dm1.created_at')
						   ->from('dashboard_menu as dm1')
						   ->join('dashboard_menu as dm2', 'dm1.parent_menu_id = dm2.id', 'left')
						   ->join('auth_user as au', 'dm1.created_by = au.id')
						//    ->where($where)
						   ->order_by('dm1.order_by')
						   ->get();

		if ($result->num_rows() > 0) {
			return $result->result();

		} else {
			return false;
		}		
	}

	/**
	 * This function populates all the list of user group. But if the function is being called from the manage_user_group controller function, 'true' boolean value is passed through the param, for which all the user group list is populated except for 'Admin' user group. This is done because superadmin of the system is also mapped with the 'Admin' user group. So, it must not be edited or deleted from the manage user group section.
	 *
	 * @param boolean $manage_user_group
	 * @return void
	 */
	public function getUserGroupList($manage_user_group = false)
	{
		if ($manage_user_group) {
			$where_not = array(1); // Id of 'Admin' user group
			$this->db->where_not_in('id', $where_not); // except admin group, populates all user group list
		}
		
		$result = $this->db->get('auth_group');

		if ($result->num_rows() > 0) {
			return $result->result();

		} else {
			return false;
		}
	}

	public function getUserGroupById($group_id)
	{
		$where = array(
			'id' => $group_id
		);

		$result = $this->db->get_where('auth_group', $where);

		if ($result->num_rows() == 1) {
			return $result->row();

		} else {
			return false;
		}
	}

	public function insertUserGroup($insert_data)
	{
		return $this->db->insert('auth_group', $insert_data);
	}

	public function updateUserGroup($group_id, $update_data)
	{
		$where = array(
			'id' => $group_id
		);

		$this->db->where($where);
		$this->db->update('auth_group', $update_data);

		if ($this->db->affected_rows() == 1) {
            return true;
            
        } else {
            return false;
        }
	}

	public function deleteUserGroup($group_id)
	{
		$where = array(
			'id' => $group_id
		);

		$this->db->where($where);
		$this->db->delete('auth_group');

		if ($this->db->affected_rows() == 1) {
            return true;
            
        } else {
            return false;
        }
	}

	public function menuDetailById($menu_id)
	{
		$where = array(
			'id' => $menu_id
		);

		$result = $this->db->get_where('dashboard_menu', $where);

		if ($result->num_rows() == 1) {
			return $result->row();

		} else {
			return false;
		}
	}

	public function updateDashboardMenu($menu_id, $update_data)
	{
		$where = array(
			'id' => $menu_id
		);

		$this->db->where($where);
		$this->db->update('dashboard_menu', $update_data);

		if ($this->db->affected_rows() == 1) {
            return true;
            
        } else {
            return false;
        }
	}

	public function deleteDashboardMenu($menu_id)
	{
		$where = array(
			'id' => $menu_id
		);

		$this->db->where($where);
		$this->db->delete('dashboard_menu');

		if ($this->db->affected_rows() == 1) {
            return true;
            
        } else {
            return false;
        }
	}

	public function getUsersList()
	{
		$where_not = array(1);

		$result = $this->db->select('au.id as user_id, au.username, au.email, au.active_status, ud.firstname, ud.lastname, ag.group_name')
						   ->from('auth_user as au')
						   ->join('user_detail as ud', 'au.id = ud.user_id')
						   ->join('auth_user_group_mapping as ugm', 'au.id = ugm.user_id', 'left')
						   ->join('auth_group as ag', 'ag.id = ugm.group_id', 'left')
						   ->where_not_in('au.is_super_admin', $where_not)
						   ->order_by('au.id')
						   ->get();

		if ($result->num_rows() > 0) {
			return $result->result();

		} else {
			return false;
		}				
	}

	public function getUserDetailById($user_id)
	{
		$where = array(
			'au.id' => $user_id
		);

		$result = $this->db->select('au.id as user_id, au.username, au.email, au.password, au.active_status, ud.firstname, ud.lastname, ud.gender, ag.id as group_id, ag.group_name')
						   ->from('auth_user as au')
						   ->join('user_detail as ud', 'au.id = ud.user_id')
						   ->join('auth_user_group_mapping as ugm', 'au.id = ugm.user_id', 'left')
						   ->join('auth_group as ag', 'ag.id = ugm.group_id', 'left')
						   ->where($where)
						   ->get();

		if ($result->num_rows() == 1) {
			return $result->row();

		} else {
			return false;
		}	
	}

	public function insertAuthUser($insert_auth_user)
	{
		$this->db->insert('auth_user', $insert_auth_user);
		$insert_id = $this->db->insert_id();

   		return  $insert_id;
	}

	public function insertUserDetail($insert_user_detail)
	{
		return $this->db->insert('user_detail', $insert_user_detail);
	}

	public function insertUserGroupMapping($insert_user_group_map)
	{
		return $this->db->insert('auth_user_group_mapping', $insert_user_group_map);
	}

	public function checkEmail($email, $user_id)
	{
		$where = array(
			'email' => $email
		);

		$this->db->where_not_in('ID', $user_id);
		$result = $this->db->get_where('auth_user', $where);

		if ($result->num_rows() == 1) {
			return $result->row();

		} else {
			return false;
		}	
	}

	public function checkUsername($username, $user_id)
	{
		$where = array(
			'username' => $username
		);

		$this->db->where_not_in('id', $user_id);
		$result = $this->db->get_where('auth_user', $where);

		if ($result->num_rows() == 1) {
			return $result->row();
		} else {
			return false;
		}	
	}

	public function updateAuthUser($update_data, $user_id)
	{
		$where = array(
			'id' => $user_id
		);

		$this->db->where($where);
		return $this->db->update('auth_user', $update_data);
	}

	public function updateUserDetail($update_data, $user_id)
	{
		$where = array(
			'user_id' => $user_id
		);

		$this->db->where($where);
		return $this->db->update('user_detail', $update_data);
	}

	public function updateUserGroupMap($update_data, $user_id)
	{
		$where = array(
			'user_id' => $user_id
		);

		$this->db->where($where);
		$this->db->update('auth_user_group_mapping', $update_data);

		if ($this->db->affected_rows() == 1) {
            return true;
            
        } else {
            return false;
        }
	}

	public function deleteUser($user_id)
	{
		$where = array(
			'id' => $user_id
		);

		$this->db->where($where);
		$this->db->delete('auth_user');

		if ($this->db->affected_rows() == 1) {
            return true;
            
        } else {
            return false;
        }
	}
	
	public function getAllDashMenu()
	{
		$where = array(
			'status' => 1
		);

		$this->db->order_by('menu_name', 'asc');
		$result = $this->db->get_where('dashboard_menu', $where);
		
		if ($result->num_rows() > 0) {
			return $result->result();
		} else {
			return false;
		}
	}

	public function checkMenuAccess($userGroupId, $menuId)
	{
		$where = array(
			'group_id' => $userGroupId,
			'menu_id' => $menuId
		);

		$result = $this->db->get_where('dashboard_menu_access', $where);
		if ($this->db->affected_rows() == 1) {
            return true;
        } else {
            return false;
        }
	}

	public function insertMenuAccessLevel($insertData)
	{
		return $this->db->insert('dashboard_menu_access', $insertData);
	}

	public function updateMenuAccessLevel($userGroupId, $menuId, $updateData)
	{
		$where = array(
			'group_id' => $userGroupId,
			'menu_id' => $menuId
		);

		$this->db->where($where);
		return $this->db->update('dashboard_menu_access', $updateData);
	}

	public function getDashboardMenuAccess_old2($userGroupId, $menuId)
	{
		$where = array(
			"group_id" => $userGroupId,
			"menu_id" => $menuId
		);

		$result = $this->db->get_where("dashboard_menu_access", $where);

		if ($result->num_rows() == 1) {
			return $result->row();
		} else {
			return false;
		}	
	}

	public function getDashboardMenuAccess($userGroupId)
	{
		$where = array(
			"a.group_id" => $userGroupId
		);

		$result = $this->db->select("a.*, m.menu_name, m.menu_type")
						   ->from("dashboard_menu_access as a")
						   ->join("dashboard_menu as m", "a.menu_id = m.id")
						   ->where($where)
						   ->get();

		if ($result->num_rows() > 0) {
			return $result->result();
		} else {
			return false;
		}	
	}
}

    