<?php 

if( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * All the functionality related CMS, User Management and other backend functions are implemented in this class named 'Admin'.
 */
class Admin extends MX_Controller
{
    /*
	 *	variable to hold data for view
	 */
	private static $viewData = array();
	
	public function __construct()
	{
		parent::__construct();

		// To show the username in admin dashboard of the logged in user
		if (uri_segment(2) != 'login') {
			self::$viewData['username'] = $this->auth_model->getAuthUser()->username;
		}

		// Dashboard side menu
		self::$viewData['dashboardSideMenu'] = $this->admin_model->getDashboardSideMenu();

		// Checks if the auth active status, auth temp_status and user group login_status is 0 or not. If one of them is 0, then user is forced to logout of the system.
		if ( (uri_segment(2) != 'login') && (($this->auth_model->getAuthUser()->active_status == 0) || ($this->auth_model->getAuthUser()->temp_status == 0) || ($this->auth_model->getUserGroupById(session_data('user_id'))->login_status == 0)) ) 
		{
			$this->logout(); // force logout
		}
	}

	public function index()
	{
		show_404();
	}

	/**
	 * This function returns login page for admin-session. Also, login action is carried out in this function which is determined by checking if the respective function is called through post moethod or not.
	 *
	 * @return void
	 */
	public function login()
	{
		// Checks if logged in 
		if (isLoggedin()) {
			redirect('admin-session/dashboard');
			exit;
		}

		// Check whether the function is called through post method
		if ($this->input->post()) {
			// Form-validation
			$this->form_validation->set_rules('username', ' ', 'required');
			$this->form_validation->set_rules('password', ' ', 'required');
			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			if (!$this->form_validation->run()) {
				// If validation is false, then return to page login and show validation error. 
				$this->load->view('login');

			} else {
				$username = $this->input->post('username');
				$password = $this->input->post('password');

				// Login procedure
				$authLogin = $this->auth_model->authLogin($username, $password);

				// Check if all the conditions for login are satisfied or not.
				if ($authLogin == false) {
					$message = 'Username or password incorrect.';
					$this->session->set_flashdata('error_msg', $message);
					redirect('admin-session/login'); 

				} else {

					// Checks if account is active or not
					if ($authLogin == '403') {
						$message = 'Your are not authorized to access this system.';
						$this->session->set_flashdata('error_msg', $message);
						redirect('admin-session/login'); 

					} elseif ($authLogin == 'success') {
						redirect('admin-session/dashboard'); 
					}
				}
			}

		} else {
			$this->load->view('login');
		}
	}

	/**
	 * This function returns to admin dashboard page.
	 *
	 * @return void
	 */
	public function dashboard()
	{
		// Checks if not logged in
		if (!isLoggedin()) {
			redirect('admin-session/login');
			exit;
		}

		self::$viewData['title'] = "Dashboard";
		self::$viewData['breadcrumb'] = "Dashboard"; 
		self::$viewData['page'] = "dashboard";
		$this->load->view(TEMPL_ADMIN, self::$viewData);
	}

	/**
	 * Through this function, change password form view page is loaded.
	 *
	 * @return void
	 */
	public function passwordSettingView()
	{
		// Checks if not logged in 
		if (!isLoggedin()) {
			redirect('admin-session/login');
			exit;
		}

		self::$viewData['title'] = "Change Password";
		self::$viewData['breadcrumb'] = "Change Password"; 
		self::$viewData['page'] = "change_password";
		$this->load->view(TEMPL_ADMIN, self::$viewData);
	}

	/**
	 * Through this function, user profile page is displayed.
	 *
	 * @return void
	 */
	public function userProfile()
	{
		// Checks if not logged in 
		if (!isLoggedin()) {
			redirect('admin-session/login');
			exit;
		}

		self::$viewData['title'] = "User Profile";
		self::$viewData['breadcrumb'] = "User Profile"; 
		// gets both auth user data and user details
		self::$viewData['user'] = $this->admin_model->getAuthUserDetail(); 
		// user group data is fetched for the purpose of getting profile update access
		self::$viewData['userGroup'] = $this->admin_model->getUserGroupById(session_data("group_id")); 
		self::$viewData['page'] = "user_profile";
		$this->load->view(TEMPL_ADMIN, self::$viewData);
	}

	/**
	 * Through this function, user profile update page is displayed. Also, after the update of user profile, the submitted form will passed in this same function for further validation and update process.
	 *
	 * @return void
	 */
	public function userProfileUpdate()
	{
		// Checks if not logged in 
		if (!isLoggedin()) {
			redirect('admin-session/login');
			exit;
		}

		self::$viewData['title'] = "User Profile";
		self::$viewData['breadcrumb'] = "User Profile"; 
		self::$viewData['user'] = $this->admin_model->getAuthUserDetail(); 
		self::$viewData['page'] = "user_profile_update";

		if ($this->input->post()) {
			$this->form_validation->set_rules('username', ' ', 'required|trim');
			$this->form_validation->set_rules('email', ' ', 'required|trim');
			$this->form_validation->set_rules('firstname', ' ', 'required|trim');
			$this->form_validation->set_rules('lastname', ' ', 'required|trim');

			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);

			} else {
				$user_id = session_data('user_id'); // user id from session

				// basic user detail
				$firstname = $this->input->post('firstname');
				$lastname = $this->input->post('lastname');
				$dob = $this->input->post('dob');
				$address = $this->input->post('address');
				$gender = $this->input->post('gender');
				$contact = $this->input->post('contact');

				// auth user data
				$username = $this->input->post('username');
				$email = $this->input->post('email');
				
				//redirect url
				$redirect_url = 'admin-session/user-profile-update'; 

				// Checking if username exists or not. If it does, throw an error message.
				$checkUsername = $this->admin_model->checkUsername($username, $user_id);
				if ($checkUsername) {
					$message = 'Username already exists.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirect_url); 
				}

				// Checking if email exists or not. If it does, throw an error message.
				$checkEmail = $this->admin_model->checkEmail($email, $user_id);
				if ($checkEmail) {
					$message = 'Email already exists.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirect_url); 
				}

				// User detail to be updated
				$update_user_detail = array(
					'firstname' => $firstname,
					'lastname' => $lastname,
					'dob' => ($dob) ? $dob : null,
					'address' => $address,
					'gender' => $gender,
					'contact' => $contact,
					'updated_by' => $user_id,
					'updated_at' => date('Y-m-d H:i:s')
				);

				// Auth Data to be updated
				$update_auth_user = array(
					'username' => $username,
					'email' => $email,
					'updated_by' => $user_id,
					'updated_at' => date('Y-m-d H:i:s')
				);
				
				$updateAuthUser = $this->admin_model->updateAuthUser($update_auth_user, $user_id);
				$updateUserDetail = $this->admin_model->updateUserDetail($update_user_detail, $user_id);

				// Checks if the update is carried out successfully or not
				if ($updateAuthUser && $updateUserDetail) {
					$message = 'Profile has been updated.';
					$this->session->set_flashdata('success_msg', $message);
					redirect($redirect_url); 

				} else {
					$message = 'Profile update failed.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirect_url); 
				}
			}

		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	/**
	 * Undocumented function
	 *
	 * @return void
	 */
	public function manage_menu()
	{
		// Checks if not logged in 
		if (!isLoggedin()) {
			redirect('admin-session/login');
			exit;
		}
		
		self::$viewData['dashboardMenuId'] = $dashboardMenuId = 7;
		self::$viewData['title'] = "Dashboard Menu List";
		self::$viewData['breadcrumb'] = "Manage Dashboard Menu"; 
		self::$viewData['heading'] = "Dashboard Menu List";
		self::$viewData['dashMenuList'] = $this->admin_model->getDashboardMenuList();
		self::$viewData['page'] = "dashboard_menu/menu_list";
		$this->load->view(TEMPL_ADMIN, self::$viewData);
	}

	public function dashboard_menu_add()
	{
		// Checks if not logged in 
		if (!isLoggedin()) {
			redirect('admin-session/login');
			exit;
		}

		self::$viewData['title'] = "Add Dashboard Menu";
		self::$viewData['breadcrumb'] = "Manage Dashboard Menu";
		self::$viewData['heading'] = "Add Dashboard Menu"; 
		self::$viewData['parent_menu'] = $this->admin_model->getParentMenu(); 
		self::$viewData['page'] = "dashboard_menu/add_menu_form";

		// Check whether the function is called through post method
		if ($this->input->post()) {
			$this->form_validation->set_rules('menu_type', ' ', 'required|trim');

			if ( ($this->input->post('menu_type') != 'p') && ($this->input->post('menu_type') != 's') ) {
				$this->form_validation->set_rules('parent_menu', ' ', 'required|trim');
			}

			if ($this->input->post('menu_type') != 'p') {
				$this->form_validation->set_rules('menu_uri', ' ', 'required|trim');
			}

			$this->form_validation->set_rules('name', ' ', 'required|trim');
			$this->form_validation->set_rules('slug', ' ', 'required|trim');
			$this->form_validation->set_rules('menu_icon', ' ', 'required|trim');
			$this->form_validation->set_rules('order_by', ' ', 'required|trim');
			$this->form_validation->set_rules('status', ' ', 'required|trim');

			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);

			} else {
				$menu_type = $this->input->post('menu_type');
				$parent_menu = ( ($menu_type != 'p') && ($menu_type != 's') ) ? $this->input->post('parent_menu') : NULL;
				$menu_uri = $this->input->post('menu_uri');
				$menu_name = $this->input->post('name');
				$slug = $this->input->post('slug');
				$menu_icon = $this->input->post('menu_icon');
				$order_by = $this->input->post('order_by');
				$status = $this->input->post('status');

				$insert_data = array(
					'menu_type' => $menu_type,
					'parent_menu_id' => $parent_menu,
					'menu_name' => $menu_name,
					'slug' => $slug,
					'menu_uri' => $menu_uri,
					'menu_icon' => $menu_icon,
					'order_by' => $order_by,
					'status' => $status,
					'created_by' => session_data('user_id'),
				);

				$insertDashboardMenu = $this->admin_model->insertDashboardMenu($insert_data);
				$redirect_url = 'admin-session/manage-menu'; //redirect url

				if ($insertDashboardMenu) {
					$message = 'Dashboard menu has been added.';
					$this->session->set_flashdata('success_msg', $message);
					redirect($redirect_url); 

				} else {
					$message = 'Failed to add dashboard menu.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirect_url); 
				}
			}

		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	public function dashboard_menu_edit($menu_id)
	{
		// Checks if not logged in 
		if (!isLoggedin()) {
			redirect('admin-session/login');
			exit;
		}

		self::$viewData['title'] = "Add Dashboard Menu";
		self::$viewData['breadcrumb'] = "Manage Dashboard Menu";
		self::$viewData['heading'] = "Add Dashboard Menu"; 
		self::$viewData['parent_menu'] = $this->admin_model->getParentMenu(); 
		self::$viewData['detail'] = $this->admin_model->menuDetailById($menu_id);
		self::$viewData['page'] = "dashboard_menu/edit_menu_form";

		// Check whether the function is called through post method
		if ($this->input->post()) {
			$this->form_validation->set_rules('menu_type', ' ', 'required|trim');

			if ($this->input->post('menu_type') != 'p') {
				$this->form_validation->set_rules('menu_uri', ' ', 'required|trim');
			}

			if ($this->input->post('menu_type') == 'c') {
				$this->form_validation->set_rules('parent_menu', ' ', 'required|trim');
			}

			$this->form_validation->set_rules('name', ' ', 'required|trim');
			$this->form_validation->set_rules('slug', ' ', 'required|trim');
			$this->form_validation->set_rules('menu_icon', ' ', 'required|trim');
			$this->form_validation->set_rules('order_by', ' ', 'required|trim');
			$this->form_validation->set_rules('status', ' ', 'required|trim');

			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);

			} else {
				$menu_type = $this->input->post('menu_type');
				$parent_menu = ($menu_type == 'c') ? $this->input->post('parent_menu') : NULL;
				$menu_uri = $this->input->post('menu_uri');
				$menu_name = $this->input->post('name');
				$slug = $this->input->post('slug');
				$menu_icon = $this->input->post('menu_icon');
				$order_by = $this->input->post('order_by');
				$status = $this->input->post('status');

				$update_data = array(
					'menu_type' => $menu_type,
					'parent_menu_id' => $parent_menu,
					'menu_name' => $menu_name,
					'slug' => $slug,
					'menu_uri' => $menu_uri,
					'menu_icon' => $menu_icon,
					'order_by' => $order_by,
					'status' => $status,
					'updated_by' => session_data('user_id'),
					'updated_at' => date('Y-m-d H:i:s'),
				);

				$updateDashboardMenu = $this->admin_model->updateDashboardMenu($menu_id, $update_data);
				$redirect_url = 'admin-session/dashboard_menu_edit/'.$menu_id; //redirect url

				if ($updateDashboardMenu) {
					$message = 'Dashboard menu has been updated.';
					$this->session->set_flashdata('success_msg', $message);
					redirect($redirect_url); 

				} else {
					$message = 'Failed to update dashboard menu.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirect_url); 
				}
			}

		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	public function dashboard_menu_delete($menu_id)
	{
		// Checks if not logged in 
		if (!isLoggedin()) {
			redirect('admin-session/login');
			exit;
		}

		$deleteDashboardMenu = $this->admin_model->deleteDashboardMenu($menu_id);
		$redirect_url = 'admin-session/manage-menu'; //redirect url

		if ($deleteDashboardMenu) {
			$message = 'Dashboard Menu has been deleted.';
			$this->session->set_flashdata('success_msg', $message);
			redirect($redirect_url); 

		} else {
			$message = 'Failed to delete Dashboard Menu.';
			$this->session->set_flashdata('error_msg', $message);
			redirect($redirect_url); 
		}
	}

	public function manage_users()
	{
		// Checks if not logged in 
		if (!isLoggedin()) {
			redirect('admin-session/login');
			exit;
		}

		self::$viewData['dashboardMenuId'] = $dashboardMenuId = 6;
		self::$viewData['title'] = "Manage Users";
		self::$viewData['breadcrumb'] = "Manage Users"; 
		self::$viewData['heading'] = "Users List";
		self::$viewData['usersList'] = $this->admin_model->getUsersList();
		// echo '<pre>';
		// print_r(self::$viewData['usersList']); exit;
		self::$viewData['page'] = "user_management/user_list";
		$this->load->view(TEMPL_ADMIN, self::$viewData);
	}

	public function user_add()
	{
		// Checks if not logged in 
		if (!isLoggedin()) {
			redirect('admin-session/login');
			exit;
		}

		self::$viewData['title'] = "Manage User";
		self::$viewData['breadcrumb'] = "Manage User"; 
		self::$viewData['heading'] = "Add User";
		self::$viewData['userGroupList'] = $this->admin_model->getUserGroupList();
		self::$viewData['page'] = "user_management/add_user_form";

		// Check whether the function is called through post method
		if ($this->input->post()) {
			$this->form_validation->set_rules('user_group', ' ', 'required|trim');
			$this->form_validation->set_rules('first_name', ' ', 'required|trim');
			$this->form_validation->set_rules('last_name', ' ', 'required|trim');
			$this->form_validation->set_rules('gender', ' ', 'required|trim');
			$this->form_validation->set_rules('email', ' ', 'required|trim|is_unique[auth_user.EMAIL]',
				array('is_unique' => 'This email already exists.')
			);
			$this->form_validation->set_rules('username', ' ', 'required|trim|is_unique[auth_user.USERNAME]',
				array('is_unique' => 'This username already exists.')
			);
			$this->form_validation->set_rules('password', ' ', 'required|trim');

			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);

			} else {
				$user_group = $this->input->post('user_group');
				$first_name = $this->input->post('first_name');
				$last_name = $this->input->post('last_name');
				$gender = $this->input->post('gender');
				$email = $this->input->post('email');
				$username = $this->input->post('username');
				$password = $this->input->post('password');

				// This data will be inserted in table 'auth_user'
				$insert_auth_user = array(
					'email' => $email,
					'username' => $username,
					'password' => encrypt_password($password),
					'created_by' => session_data('user_id'),
				);

				$this->db->trans_begin();

				// Here last inserted user_id is returned.
				$insertAuthUser = $this->admin_model->insertAuthUser($insert_auth_user); 
				$user_id = $insertAuthUser;

				// This data will be inserted in table 'user_detail'
				$insert_user_detail = array(
					'user_id' => $user_id,
					'firstname' => $first_name,
					'lastname' => $last_name,
					'gender' => $gender,
					'created_by' => session_data('user_id'),
				);

				$insertUserDetail = $this->admin_model->insertUserDetail($insert_user_detail);

				// This data will be inserted in table 'auth_user_group_mapping'
				$insert_user_group_map = array(
					'user_id' => $user_id,
					'group_id' => $user_group,
					'created_by' => session_data('user_id'),
				);

				$insertUserGroupMapping = $this->admin_model->insertUserGroupMapping($insert_user_group_map);

				if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
				} else {
					$this->db->trans_commit();
				}

				//redirect url
				$redirect_url = 'admin-session/manage-users'; 

				if ($insertAuthUser && $insertUserDetail && $insertUserGroupMapping) {
					$message = 'A user has been added.';
					$this->session->set_flashdata('success_msg', $message);
					redirect($redirect_url); 

				} else {
					$message = 'Failed to add user.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirect_url); 
				}
			}

		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	public function user_edit($user_id)
	{
		// Checks if not logged in 
		if (!isLoggedin()) {
			redirect('admin-session/login');
			exit;
		}

		self::$viewData['title'] = "Manage User Group";
		self::$viewData['breadcrumb'] = "Manage User Group"; 
		self::$viewData['heading'] = "Edit User";
		self::$viewData['detail'] = $this->admin_model->getUserDetailById($user_id);
		// print_r(self::$viewData['detail']); exit;
		self::$viewData['userGroupList'] = $this->admin_model->getUserGroupList();
		self::$viewData['page'] = "user_management/edit_user_form";

		// Check whether the function is called through post method
		if ($this->input->post()) {
			$this->form_validation->set_rules('user_group', ' ', 'required|trim');
			$this->form_validation->set_rules('first_name', ' ', 'required|trim');
			$this->form_validation->set_rules('last_name', ' ', 'required|trim');
			$this->form_validation->set_rules('gender', ' ', 'required|trim');
			$this->form_validation->set_rules('email', ' ', 'required|trim');
			$this->form_validation->set_rules('username', ' ', 'required|trim');
			$this->form_validation->set_rules('password', ' ', 'required|trim');
			$this->form_validation->set_rules('status', ' ', 'required|trim');

			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);

			} else {
				$user_group = $this->input->post('user_group');
				$first_name = $this->input->post('first_name');
				$last_name = $this->input->post('last_name');
				$gender = $this->input->post('gender');
				$email = $this->input->post('email');
				$username = $this->input->post('username');
				$password = $this->input->post('password');
				$status = $this->input->post('status');

				//redirect url
				$redirect_url = 'admin-session/user-edit/'.$user_id; 

				// Checking if email exists or not. If it does, provide an error message.
				$checkEmail = $this->admin_model->checkEmail($email, $user_id);
				if ($checkEmail) {
					$message = 'Email already exists.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirect_url); 
				}

				// Checking if username exists or not. If it does, provide an error message.
				$checkUsername = $this->admin_model->checkUsername($username, $user_id);
				if ($checkUsername) {
					$message = 'Username already exists.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirect_url); 
				}

				// This data will be updated in table 'auth_user'
				$update_auth_user = array(
					'email' => $email,
					'username' => $username,
					'password' => encrypt_password($password),
					'active_status' => $status,
					'updated_by' => session_data('user_id'),
					'updated_at' => date('Y-m-d H:i:s'),
				);
				
				// This data will be updated in table 'user_detail'
				$update_user_detail = array(
					'user_id' => $user_id,
					'firstname' => $first_name,
					'lastname' => $last_name,
					'gender' => $gender,
					'updated_by' => session_data('user_id'),
					'updated_at' => date('Y-m-d H:i:s'),
				);
				
				// This data will be updated in table 'auth_user_group_mapping'
				$update_user_group_map = array(
					'group_id' => $user_group,
					'updated_by' => session_data('user_id'),
					'updated_at' => date('Y-m-d H:i:s'),
				);

				// $this->db->trans_begin();

				// updates user's auth data in table 'auth_user'
				$updateAuthUser = $this->admin_model->updateAuthUser($update_auth_user, $user_id);	

				// checks if user main is updated or not
				if ($updateAuthUser) {
					// updates user's detail data in table 'user_detail'
					$updateUserDetail = $this->admin_model->updateUserDetail($update_user_detail, $user_id);

					// checks if user detail is updated or not
					if ($updateUserDetail) {
						// updates user group mapping data in table 'auth_user_group_mapping'
						$updateUserGroupMap = $this->admin_model->updateUserGroupMap($update_user_group_map, $user_id);
						
						// checks if user group mapping is updated or not
						if ($updateUserGroupMap) {
							// echo 'ok'; exit;
							$message = 'User detail has been updated.';
							$this->session->set_flashdata('success_msg', $message);
							redirect($redirect_url); 

						} else {
							// This data will be inserted in table 'auth_user_group_mapping'
							$insert_user_group_map = array(
								'user_id' => $user_id,
								'group_id' => $user_group,
								'created_by' => session_data('user_id'),
							);
							$insertUserGroupMapping = $this->admin_model->insertUserGroupMapping($insert_user_group_map);

							if ($insertUserGroupMapping) {
								$message = 'User detail has been updated.';
								$this->session->set_flashdata('success_msg', $message);
								redirect($redirect_url); 

							} else {
								$message = 'Failed to update user detail.';
								$this->session->set_flashdata('error_msg', $message);
								redirect($redirect_url); 
							}
						}

					} else {
						$message = 'Failed to update user detail.';
						$this->session->set_flashdata('error_msg', $message);
						redirect($redirect_url); 
					}

				} else {
					$message = 'Failed to update user detail.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirect_url); 
				}

				// if ($this->db->trans_status() === FALSE) {
				// 	$this->db->trans_rollback();
				// } else {
				// 	$this->db->trans_commit();
				// }
			}

		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	public function user_delete($user_id)
	{
		// Checks if not logged in 
		if (!isLoggedin()) {
			redirect('admin-session/login');
			exit;
		}

		$deleteUser = $this->admin_model->deleteUser($user_id);
		$redirect_url = 'admin-session/manage-users'; //redirect url

		if ($deleteUser) {
			$message = 'User has been deleted.';
			$this->session->set_flashdata('success_msg', $message);
			redirect($redirect_url); 

		} else {
			$message = 'Failed to delete user.';
			$this->session->set_flashdata('error_msg', $message);
			redirect($redirect_url); 
		}
	}

	public function manage_user_group()
	{
		// Checks if not logged in 
		if (!isLoggedin()) {
			redirect('admin-session/login');
			exit;
		}

		self::$viewData['dashboardMenuId'] = $dashboardMenuId = 3; // this dashboard menu id is of manage user group menu. it is used for user authorization purpose (crud access)
		self::$viewData['title'] = "Manage User Group";
		self::$viewData['breadcrumb'] = "Manage User Group"; 
		self::$viewData['heading'] = "User Group List";
		self::$viewData['userGroupList'] = $this->admin_model->getUserGroupList(true);
		self::$viewData['page'] = "user_management/user_group_list";
		$this->load->view(TEMPL_ADMIN, self::$viewData);
	}

	public function user_group_add()
	{
		// Checks if not logged in 
		if (!isLoggedin()) {
			redirect('admin-session/login');
			exit;
		}

		self::$viewData['title'] = "Manage User Group";
		self::$viewData['breadcrumb'] = "Manage User Group"; 
		self::$viewData['heading'] = "Add User Group";
		// self::$viewData['userGroupList'] = $this->admin_model->getUserGroupList();
		self::$viewData['page'] = "user_management/add_user_group_form";

		// Check whether the function is called through post method
		if ($this->input->post()) {
			$this->form_validation->set_rules('group_name', ' ', 'required|trim');
			$this->form_validation->set_rules('description', ' ', 'required|trim');

			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);

			} else {

				$group_name = $this->input->post('group_name');
				$description = $this->input->post('description');

				$insert_data = array(
					'group_name' => $group_name,
					'description' => $description
				);

				$insertUserGroup = $this->admin_model->insertUserGroup($insert_data);
				$redirect_url = 'admin-session/manage-user-group'; //redirect url

				if ($insertUserGroup) {
					$message = 'User Group has been added.';
					$this->session->set_flashdata('success_msg', $message);
					redirect($redirect_url); 

				} else {
					$message = 'Failed to add User Group.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirect_url); 
				}
			}

		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	public function user_group_edit($group_id)
	{
		// Checks if not logged in 
		if (!isLoggedin()) {
			redirect('admin-session/login');
			exit;
		}

		self::$viewData['title'] = "Manage User Group";
		self::$viewData['breadcrumb'] = "Manage User Group"; 
		self::$viewData['heading'] = "Edit User Group";
		self::$viewData['detail'] = $this->admin_model->getUserGroupById($group_id);
		self::$viewData['page'] = "user_management/edit_user_group_form";

		// Check whether the function is called through post method
		if ($this->input->post()) {
			$this->form_validation->set_rules('group_name', ' ', 'required|trim');
			$this->form_validation->set_rules('description', ' ', 'required|trim');

			$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);

			} else {

				$group_name = $this->input->post('group_name');
				$description = $this->input->post('description');
				$profile_update_access = $this->input->post('profile_update_access');
				$login_status = $this->input->post('login_status');
				$status = $this->input->post('status');

				$update_data = array(
					'group_name' => $group_name,
					'description' => $description,
					'profile_update_access' => $profile_update_access,
					'login_status' => $login_status,
					'status' => $status,
				);

				$updateUserGroup = $this->admin_model->updateUserGroup($group_id, $update_data);
				$redirect_url = 'admin-session/user-group-edit/'.$group_id; //redirect url

				if ($updateUserGroup) {
					$message = 'User Group has been updated.';
					$this->session->set_flashdata('success_msg', $message);
					redirect($redirect_url); 

				} else {
					$message = 'No changes were made.';
					$this->session->set_flashdata('error_msg', $message);
					redirect($redirect_url); 
				}
			}

		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	public function user_group_delete($group_id)
	{
		// Checks if not logged in 
		if (!isLoggedin()) {
			redirect('admin-session/login');
			exit;
		}

		$deleteUserGroup = $this->admin_model->deleteUserGroup($group_id);
		$redirect_url = 'admin-session/manage-user-group'; //redirect url

		if ($deleteUserGroup) {
			$message = 'User Group has been deleted.';
			$this->session->set_flashdata('success_msg', $message);
			redirect($redirect_url); 

		} else {
			$message = 'Failed to delete User Group.';
			$this->session->set_flashdata('error_msg', $message);
			redirect($redirect_url); 
		}
	}

	public function manage_menu_access()
	{
		// Checks if not logged in 
		if (!isLoggedin()) {
			redirect('admin-session/login');
			exit;
		}

		self::$viewData['title'] = "Manage Menu Access";
		self::$viewData['breadcrumb'] = "Manage Menu Access"; 
		self::$viewData['heading'] = "User Group/Menu Access";
		self::$viewData['userGroupList'] = $this->admin_model->getUserGroupList();
		self::$viewData['dashboardMenu'] = $this->admin_model->getAllDashMenu();
		self::$viewData["menuAccess"] = null;
		self::$viewData['page'] = "user_management/manage_menu_access";

		// Check whether the function is called through post method
		if ($this->input->post()) {
			$this->form_validation->set_rules('user_group', ' ', 'required|trim');
			$this->form_validation->set_error_delimiters('<span id="error_msg" style="color: red; font-size: 14px">', '</span>');

			if ($this->form_validation->run() == FALSE) {
				// If validation is false, then return to the respective view page as defined above and show validation error. 
				$this->load->view(TEMPL_ADMIN, self::$viewData);

			} else {
				$dashboardMenu = $this->admin_model->getAllDashMenu();
				$group_id = $this->input->post('user_group');
				// print_r($this->input->post());exit;

				foreach ($dashboardMenu as $key => $menu) {
					$view_field = 'view_'.$menu->id;
					$add_field = 'add_'.$menu->id;
					$edit_field = 'edit_'.$menu->id;
					$delete_field = 'delete_'.$menu->id;
					
					$view_access = $this->input->post($view_field);
					$add_access = $this->input->post($add_field);
					$edit_access = $this->input->post($edit_field);
					$delete_access = $this->input->post($delete_field);

					$data = array(
						'group_id' => $group_id,
						'menu_id' => $menu->id,
						'view_access' => ($view_access) ? $view_access : 0,
						'add_access' => ($add_access) ? $add_access : 0,
						'edit_access' => ($edit_access) ? $edit_access : 0,
						'delete_access' => ($delete_access) ? $delete_access : 0,
					);
					$final_data[] = $data;
				}

				$count = count($final_data);

				for ($i=0; $i < $count; $i++) 
				{ 
					$group_id = $final_data[$i]['group_id'];
					$menu_id = $final_data[$i]['menu_id'];
					$view_access = $final_data[$i]['view_access'];
					$add_access = $final_data[$i]['add_access'];
					$edit_access = $final_data[$i]['edit_access'];
					$delete_access = $final_data[$i]['delete_access'];

					$checkMenuAccess = $this->admin_model->checkMenuAccess($group_id, $menu_id);
					if ($checkMenuAccess == false) {
						$insert_data = array(
							'group_id' => $group_id,
							'menu_id' => $menu_id,
							'view_access' => $view_access,
							'add_access' => $add_access,
							'edit_access' => $edit_access,
							'delete_access' => $delete_access,
							'created_by' => session_data('user_id'),
						);
						$this->admin_model->insertMenuAccessLevel($insert_data);

					} else {
						$update_data = array(
							'view_access' => $view_access,
							'add_access' => $add_access,
							'edit_access' => $edit_access,
							'delete_access' => $delete_access,
							'updated_by' => session_data('user_id'),
							'updated_at' => date('Y-m-d H:i:s'),
						);
						$this->admin_model->updateMenuAccessLevel($group_id, $menu_id, $update_data);
					}
				}
				$redirect_url = 'admin-session/manage-menu-access'; //redirect url
				$message = 'Menu Access Level has been updated.';
				$this->session->set_flashdata('success_msg', $message);
				redirect($redirect_url); 
			}

		} else {
			$this->load->view(TEMPL_ADMIN, self::$viewData);
		}
	}

	public function ajaxLoadMenuAccess()
	{
		try {
			if ($this->input->is_ajax_request()) {
				$userGroupId = $this->input->post("userGroupId"); 

				if ($userGroupId) {
					self::$viewData["menuAccess"] = $this->admin_model->getDashboardMenuAccess($userGroupId);
					self::$viewData['dashboardMenu'] = $this->admin_model->getAllDashMenu();
					
					$menuAccessView = $this->load->view("admin/ajax_view/dashboard_menu_access", self::$viewData, TRUE);

					$response = array(
						"status" => "success",
						"menu_access_view" => $menuAccessView
					);
					header("Content-type: application/json");
					echo json_encode($response); exit;

				} else {
					$response = array(
						"status" => "error",
						"message" => "User Group field not selected."
					);
					header("Content-type: application/json");
					echo json_encode($response); exit;
				}
				
			} else {
				exit("No direct script allowed!");
			}

		} catch (Exception $e) {
			echo "Caught exception: ". $e->getMessage();
		}
	}

	/**
	 * This function logs out from current user session.
	 *
	 * @return void
	 */
	public function logout()
	{
		$redirect_path = "admin-session/login";
		$this->auth_model->authLogout($redirect_path);
	}

	







	// public function manage_menu_access_old()
	// {
	// 	// Checks if not logged in 
	// 	if (!isLoggedin()) {
	// 		redirect('admin-session/login');
	// 		exit;
	// 	}

	// 	self::$viewData['title'] = "Manage Menu Access";
	// 	self::$viewData['breadcrumb'] = "Manage Menu Access"; 
	// 	self::$viewData['heading'] = "User Group/Menu Access";
	// 	self::$viewData['userGroupList'] = $this->admin_model->getUserGroupList();
	// 	self::$viewData['allDashMenu'] = $this->admin_model->getAllDashMenu();
	// 	self::$viewData['page'] = "user_management/manage_menu_access";

	// 	// Check whether the function is called through post method
	// 	if ($this->input->post()) {
	// 		$this->form_validation->set_rules('user_group', ' ', 'required|trim');
	// 		$this->form_validation->set_error_delimiters('<span style="color: red; font-size: 14px">', '</span>');

	// 		if ($this->form_validation->run() == FALSE) {
	// 			// If validation is false, then return to the respective view page as defined above and show validation error. 
	// 			$this->load->view(TEMPL_ADMIN, self::$viewData);

	// 		} else {
	// 			$dash_menu = $this->admin_model->getAllDashMenu();
	// 			$group_id = $this->input->post('user_group');
	// 			// print_r($this->input->post());

	// 			foreach ($dash_menu as $key => $menu) {
	// 				$view_field = 'view_'.$menu->id;
	// 				$add_field = 'add_'.$menu->id;
	// 				$edit_field = 'edit_'.$menu->id;
	// 				$delete_field = 'delete_'.$menu->id;
					
	// 				$view_access = $this->input->post($view_field);
	// 				$add_access = $this->input->post($add_field);
	// 				$edit_access = $this->input->post($edit_field);
	// 				$delete_access = $this->input->post($delete_field);

	// 				$data = array(
	// 					'group_id' => $group_id,
	// 					'menu_id' => $menu->id,
	// 					'view_access' => ($view_access) ? $view_access : 0,
	// 					'add_access' => ($add_access) ? $add_access : 0,
	// 					'edit_access' => ($edit_access) ? $edit_access : 0,
	// 					'delete_access' => ($delete_access) ? $delete_access : 0,
	// 				);

	// 				$final_data[] = $data;

	// 			}

	// 			$count = count($final_data);

	// 			for ($i=0; $i < $count; $i++) 
	// 			{ 
	// 				$group_id = $final_data[$i]['group_id'];
	// 				$menu_id = $final_data[$i]['menu_id'];
	// 				$view_access = $final_data[$i]['view_access'];
	// 				$add_access = $final_data[$i]['add_access'];
	// 				$edit_access = $final_data[$i]['edit_access'];
	// 				$delete_access = $final_data[$i]['delete_access'];

	// 				$checkAccessLevel = $this->admin_model->checkAccessLevel($group_id, $menu_id);

	// 				if ($checkAccessLevel == false) {
	// 					$insert_data = array(
	// 						'group_id' => $group_id,
	// 						'menu_id' => $menu_id,
	// 						'view_access' => $view_access,
	// 						'add_access' => $add_access,
	// 						'edit_access' => $edit_access,
	// 						'delete_access' => $delete_access,
	// 						'created_by' => session_data('user_id'),
	// 					);
		
	// 					$this->admin_model->insertMenuAccessLevel($insert_data);

	// 				} else {
	// 					$update_data = array(
	// 						'view_access' => $view_access,
	// 						'add_access' => $add_access,
	// 						'edit_access' => $edit_access,
	// 						'delete_access' => $delete_access,
	// 						'updated_by' => session_data('user_id'),
	// 						'updated_at' => date('Y-m-d H:i:s'),
	// 					);

	// 					$this->admin_model->updateMenuAccessLevel($group_id, $menu_id, $update_data);
	// 				}
	// 			}

	// 			$redirect_url = 'admin-session/manage-menu-access'; //redirect url

	// 			$message = 'Menu Access Level has been updated.';
	// 			$this->session->set_flashdata('success_msg', $message);
	// 			redirect($redirect_url); 
	// 		}

	// 	} else {
	// 		$this->load->view(TEMPL_ADMIN, self::$viewData);
	// 	}
	// }



	// public function manage_menu_access_old2()
	// {
	// 	// Checks if not logged in 
	// 	if (!isLoggedin()) {
	// 		redirect('admin-session/login');
	// 		exit;
	// 	}

	// 	self::$viewData['title'] = "Manage Menu Access";
	// 	self::$viewData['breadcrumb'] = "Manage Menu Access"; 
	// 	self::$viewData['heading'] = "User Group/Menu Access";
	// 	self::$viewData['userGroupList'] = $this->admin_model->getUserGroupList();
	// 	self::$viewData['dashboardMenu'] = $this->admin_model->getAllDashMenu();
	// 	self::$viewData['page'] = "user_management/manage_menu_access";

	// 	// Check whether the function is called through post method
	// 	if ($this->input->post()) {
	// 		$this->form_validation->set_rules('user_group', ' ', 'required|trim');
	// 		$this->form_validation->set_rules('menu', ' ', 'required|trim');
	// 		$this->form_validation->set_error_delimiters('<span id="error_msg" style="color: red; font-size: 14px">', '</span>');

	// 		if ($this->form_validation->run() == FALSE) {
	// 			// If validation is false, then return to the respective view page as defined above and show validation error. 
	// 			$this->load->view(TEMPL_ADMIN, self::$viewData);

	// 		} else {
	// 			$userGroupId = $this->input->post("user_group");
	// 			$menuId = $this->input->post("menu");
	// 			$viewAccess = $this->input->post("view");
	// 			$addAccess = $this->input->post("add");
	// 			$editAccess = $this->input->post("edit");
	// 			$deleteAccess = $this->input->post("delete");

	// 			$checkMenuAccess = $this->admin_model->checkMenuAccess($userGroupId, $menuId);
	// 			if ($checkMenuAccess == false) {
	// 				$insertData = array(
	// 					'group_id' => $userGroupId,
	// 					'menu_id' => $menuId,
	// 					'view_access' => $viewAccess ? $viewAccess : 0,
	// 					'add_access' => $addAccess ? $addAccess : 0,
	// 					'edit_access' => $editAccess ? $editAccess : 0,
	// 					'delete_access' => $deleteAccess ? $deleteAccess : 0,
	// 					'created_by' => session_data('user_id'),
	// 				);

	// 				$this->admin_model->insertMenuAccessLevel($insertData);

	// 			} else {
	// 				$updateData = array(
	// 					'view_access' => $viewAccess ? $viewAccess : 0,
	// 					'add_access' => $addAccess ? $addAccess : 0,
	// 					'edit_access' => $editAccess ? $editAccess : 0,
	// 					'delete_access' => $deleteAccess ? $deleteAccess : 0,
	// 					'updated_by' => session_data('user_id'),
	// 					'updated_at' => date('Y-m-d H:i:s'),
	// 				);
	// 				$this->admin_model->updateMenuAccessLevel($userGroupId, $menuId, $updateData);
	// 			}

	// 			$redirect_url = 'admin-session/manage-menu-access'; //redirect url
	// 			$message = 'Menu Access Level has been updated.';
	// 			$this->session->set_flashdata('success_msg', $message);
	// 			redirect($redirect_url); 
	// 		}

	// 	} else {
	// 		$this->load->view(TEMPL_ADMIN, self::$viewData);
	// 	}
	// }



	// public function ajaxLoadMenuAccess_old2()
	// {
	// 	try {
	// 		if ($this->input->is_ajax_request()) {
	// 			$userGroupId = $this->input->post("userGroupId"); 
	// 			$menuId = $this->input->post("menuId"); 

	// 			if ($userGroupId && $menuId) {
	// 				self::$viewData["menuAccess"] = $this->admin_model->getDashboardMenuAccess($userGroupId, $menuId);

	// 				self::$viewData["menuDetail"] = $this->admin_model->menuDetailById($menuId);
					
	// 				$menuAccessView = $this->load->view("admin/ajax_view/dashboard_menu_access", self::$viewData, TRUE);

	// 				$response = array(
	// 					"status" => "success",
	// 					"menu_access_view" => $menuAccessView
	// 				);
	// 				header("Content-type: application/json");
	// 				echo json_encode($response); exit;

	// 			} else {
	// 				$response = array(
	// 					"status" => "error",
	// 					"message" => "Both user-group and menu needs to be selected."
	// 				);
	// 				header("Content-type: application/json");
	// 				echo json_encode($response); exit;
	// 			}
				
	// 		} else {
	// 			exit("No direct script allowed!");
	// 		}

	// 	} catch (Exception $e) {
	// 		echo "Caught exception: ". $e->getMessage();
	// 	}
	// }

	
}