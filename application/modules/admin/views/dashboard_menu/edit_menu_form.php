<!-- left column -->
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card">

        <?php
            $action = base_url('admin-session/dashboard-menu-edit/'.$detail->id);
            $attributes = array(
                "id"        => "edit_dash_menu", 
                "name"      => "edit_dash_menu",
                "method"    => "POST"
            );

            echo form_open($action, $attributes); 
        ?>
        
        <div class="col-md-12 card-body">

            <h4><?php echo $heading ?> <a href="<?php echo base_url('admin-session/manage-menu') ?>" class="btn btn-success btn-sm float-right" data-toggle="tooltip" data-placement="top" title="Back to List"><span class="fa fa-arrow-left"></span></a></h4><hr>

            <div class="col-md-6">
                <?php if($this->session->flashdata('error_msg')): ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Sorry!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
                    </div>
                <?php endif; ?>

                <?php if($this->session->flashdata('success_msg')): ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success!</strong> <?php echo $this->session->flashdata('success_msg'); ?>
                    </div>
                <?php endif; ?>
            </div>

            <div class="form-group row">
                <div class="col-md-3">
                    <label>Select Menu Type <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <div class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" id="parent" name="menu_type" value="p" <?php echo ($detail->menu_type == 'p') ? 'checked' : ''; ?> >
                        <label for="parent" class="custom-control-label">Parent</label>
                    </div>

                    <div class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" id="child" name="menu_type"  value="c" <?php echo ($detail->menu_type == 'c') ? 'checked' : ''; ?> >
                        <label for="child" class="custom-control-label">Child</label>
                    </div>

                    <div class="custom-control custom-radio">
                        <input class="custom-control-input" type="radio" id="single" name="menu_type"  value="s" <?php echo ($detail->menu_type == 's') ? 'checked' : ''; ?> >
                        <label for="single" class="custom-control-label">Single</label>
                    </div>
                    <?php echo form_error('menu_type'); ?>
                </div>
            </div>

            <div class="form-group row select_parent_menu_sec">
                <div class="col-md-3">
                    <label for="parent_menu">Select Parent Menu <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <select class="form-control" name="parent_menu" id="parent_menu">
                        <?php if ($parent_menu): ?>
                            <option value="">--- Select Parent Menu ---</option>
                            <?php foreach ($parent_menu as $key => $menu): ?>
                                <option value="<?php echo $menu->id ?>" <?php echo ($detail->parent_menu_id == $menu->id) ? 'selected' : ''; ?> ><?php echo $menu->menu_name ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                    <?php echo form_error('parent_menu'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-3">
                    <label for="name">Menu Name <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Enter Menu Name" value="<?php echo $detail->menu_name ?>" >
                    <?php echo form_error('name'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-3">
                    <label for="slug">Slug <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="slug" name="slug" readonly value="<?php echo $detail->slug ?>" >
                    <?php echo form_error('slug'); ?>
                </div>
            </div>
            
            <div class="form-group row">
                <div class="col-md-3">
                    <label for="menu_uri">Menu URI <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="menu_uri" name="menu_uri" placeholder="Enter Menu URI" value="<?php echo $detail->menu_uri ?>" >
                    <?php echo form_error('menu_uri'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-3">
                    <label for="menu_icon">Menu Icon <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="menu_icon" name="menu_icon" placeholder="Example: far fa-circle (font-awesome icons)" value="<?php echo $detail->menu_icon ?>" >
                    <?php echo form_error('menu_icon'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-3">
                    <label for="order_by">Order By <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="number" class="form-control" id="order_by" name="order_by" placeholder="Enter Order By" value="<?php echo $detail->order_by ?>" >
                    <?php echo form_error('order_by'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-3">
                    <label for="status">Status <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <select class="form-control" name="status" id="status">
                        <option value="1" <?php echo ($detail->status == 1) ? 'selected' : ''; ?> >Active</option>
                        <option value="0" <?php echo ($detail->status == 0) ? 'selected' : ''; ?> >Inactive</option>
                    </select>
                    <?php echo form_error('status'); ?>
                </div>
            </div>
            <!-- <input type="hidden" name="<?php //echo $this->security->get_csrf_token_name();?>" value="<?php //echo $this->security->get_csrf_hash();?>"> -->
            
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-warning">Update</button>
        </div>

        <?php echo form_close() ?>
    </div>
    <!-- /.card -->

</div>

<script>

    $(document).ready(function(){
        // on windows load check if the value menu_type is 'p' or not. If yes, hide the section with class 'select_parent_menu_sec'.
        var menu_type = $("input:radio[name='menu_type']:checked").val();

        if (menu_type == 'p' || menu_type == 's') {
            $('.select_parent_menu_sec').hide();
        }     
    });

    // storing value of menu uri in a variable.
    var menu_uri = "<?php echo $detail->menu_uri ?>"

    // on change event of element name menu_type with radio button inputs.
    $('input:radio[name="menu_type"]').change(function(){
        if (this.checked && this.value == 'p') {
            $('.select_parent_menu_sec').slideUp();
            $('input[name="menu_uri"]').attr('readonly', true).val('#');

        } else if(this.checked && this.value == 's') {
            $('.select_parent_menu_sec').slideUp();
            $('input[name="menu_uri"]').attr('readonly', false).val(menu_uri);
             
        } else {
            $('.select_parent_menu_sec').slideDown();
            $('input[name="menu_uri"]').attr('readonly', false).val(menu_uri);
        }
    });

</script>