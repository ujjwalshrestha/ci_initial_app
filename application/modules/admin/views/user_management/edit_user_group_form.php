<!-- left column -->
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card">

        <?php
            $action = base_url('admin-session/user-group-edit/'.$detail->id);
            $attributes = array(
                "id" => "edit_user_group", 
                "name" => "edit_user_group",
                "method" => "POST"
            );

            echo form_open($action, $attributes); 
        ?>
        
        <div class="col-md-12 card-body">

            <h4><?php echo $heading ?> <a href="<?php echo base_url('admin-session/manage-user-group') ?>" class="btn btn-success btn-sm float-right" data-toggle="tooltip" data-placement="top" title="Back to List"><span class="fa fa-arrow-left"></span></a></h4><hr>

            <div class="col-md-6">
                <?php if($this->session->flashdata('error_msg')): ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <?php echo $this->session->flashdata('error_msg'); ?>
                    </div>
                <?php endif; ?>

                <?php if($this->session->flashdata('success_msg')): ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success!</strong> <?php echo $this->session->flashdata('success_msg'); ?>
                    </div>
                <?php endif; ?>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="group_name">Group Name <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="group_name" name="group_name" placeholder="Enter Group Name" value="<?php echo $detail->group_name ?>" >
                    <?php echo form_error('group_name'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="description">Description <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="description" name="description" placeholder="Enter Description" value="<?php echo $detail->description ?>" >
                    <?php echo form_error('description'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="profile_update_access">Profile Update Access <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <select class="form-control" name="profile_update_access" id="profile_update_access">
                        <option value="1" <?php echo ($detail->profile_update_access == 1) ? 'selected' : ''; ?> >Authorized</option>
                        <option value="0" <?php echo ($detail->profile_update_access == 0) ? 'selected' : ''; ?> >Not Authorized</option>
                    </select>
                    <?php echo form_error('profile_update_access'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="login_status">Login Status <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <select class="form-control" name="login_status" id="login_status">
                        <option value="1" <?php echo ($detail->login_status == 1) ? 'selected' : ''; ?> >Authorized</option>
                        <option value="0" <?php echo ($detail->login_status == 0) ? 'selected' : ''; ?> >Not Authorized</option>
                    </select>
                    <?php echo form_error('login_status'); ?>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="status">Status <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <select class="form-control" name="status" id="status">
                        <option value="1" <?php echo ($detail->status == 1) ? 'selected' : ''; ?> >Active</option>
                        <option value="0" <?php echo ($detail->status == 0) ? 'selected' : ''; ?> >Inactive</option>
                    </select>
                    <?php echo form_error('status'); ?>
                </div>
            </div>
            
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-warning">Update</button>
        </div>

        <?php echo form_close() ?>
    </div>
    <!-- /.card -->

</div>