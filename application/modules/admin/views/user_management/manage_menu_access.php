<!-- left column -->
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card">

        <?php
            $action = base_url('admin-session/manage-menu-access');
            $attributes = array(
                "id" => "manage_menu_access", 
                "name" => "manage_menu_access",
                "method" => "POST"
            );

            echo form_open($action, $attributes); 
        ?>
        
        <div class="col-md-12 card-body">

            <h4><?php echo $heading ?></h4><hr>

            <div class="col-md-7">
                <?php if($this->session->flashdata('error_msg')): ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Sorry!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
                    </div>
                <?php endif; ?>

                <?php if($this->session->flashdata('success_msg')): ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success!</strong> <?php echo $this->session->flashdata('success_msg'); ?>
                    </div>
                <?php endif; ?>
            </div>

            <div class="form-group row">
                <div class="col-md-3">
                    <label for="user_group">Select User Group <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <select class="form-control" name="user_group" id="user_group">
                        <option value="">--- Select User Group ---</option>
                        <?php if($userGroupList): ?>
                            <?php foreach($userGroupList as $key => $group): ?>
                                <option value="<?php echo $group->id ?>"><?php echo ucfirst($group->group_name) ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                    <?php echo form_error('user_group'); ?>
                </div>
            </div>

            <div class="form-group row" id="menu_access_section">
                <!-- This section is replaced with new view page through ajax call -->
                <?php $this->load->view("admin/ajax_view/dashboard_menu_access"); ?>
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-success">Submit</button>
        </div>

        <?php echo form_close() ?>
    </div>
    <!-- /.card -->
</div>

<script>
    function ajaxCall() 
    {
        var userGroupId = $("#user_group").val(),
            url = "<?php echo base_url("admin/ajaxLoadMenuAccess"); ?>",
            data = {"userGroupId":userGroupId};

        $.ajax({
            type : "post",
            url : url,
            data : data,
            dataType : 'json',
            success: function(resp) {
                if (resp.status == "success") {
                    $('#menu_access_section').html('');
                    $('#menu_access_section').html(resp.menu_access_view);
                } else if (resp.status == "error") {
                    $('.permission-checkbox').prop('checked', false);
                }
            },
            error: function() {
                alert('Internal Server Error!');
            }
        });
    }

    $(document).ready(function() {
        $(document).off("change", "#user_group").on("change", "#user_group", function(e) {
            $(this).siblings("#error_msg").hide(); // hides the validation error message
            ajaxCall();
        });
    });
</script>
