<!-- left column -->
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card">

        <?php
            $action = base_url('admin-session/manage-menu-access');
            $attributes = array(
                "id" => "manage_menu_access", 
                "name" => "manage_menu_access",
                "method" => "POST"
            );

            echo form_open($action, $attributes); 
        ?>
        
        <div class="col-md-12 card-body">

            <h4><?php echo $heading ?></h4><hr>

            <div class="col-md-6">
                <?php if($this->session->flashdata('error_msg')): ?>
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Sorry!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
                    </div>
                <?php endif; ?>

                <?php if($this->session->flashdata('success_msg')): ?>
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <strong>Success!</strong> <?php echo $this->session->flashdata('success_msg'); ?>
                    </div>
                <?php endif; ?>
            </div>

            <div class="form-group row">
                <div class="col-md-2">
                    <label for="user_group">Select User Group <span class="red-asterisk">*</span></label>
                </div>
                <div class="col-md-4">
                    <select class="form-control" name="user_group" id="user_group">
                        <option value="">--- Select User Group ---</option>
                        <?php if($userGroupList): ?>
                            <?php foreach($userGroupList as $key => $group): ?>
                                <option value="<?php echo $group->id ?>"><?php echo ucfirst($group->group_name) ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                    <?php echo form_error('user_group'); ?>
                </div>
            </div>

            <div class="form-group row" id="menu_list_section">

                <?php if($allDashMenu): ?>
                    <?php foreach($allDashMenu as $key => $menu): ?>
                        <div class="col-md-12" style="margin: 0 0 15px 0">
                            <div>
                                <i class="fas fa-caret-right"></i> <label><?php echo $menu->menu_name ?></label>
                            </div>

                            <div class="crud_section">
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" name="view_<?php echo $menu->id ?>" id="view_<?php echo $menu->id ?>" value="1">
                                    <label for="view_<?php echo $menu->id ?>" class="custom-control-label text-primary">View Content Access</label>
                                </div>

                                <!-- If the menu type is not 'parent' -->
                                <?php if ($menu->menu_type != 'p'): ?>
                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" name="add_<?php echo $menu->id ?>" id="add_<?php echo $menu->id ?>" value="1">
                                        <label for="add_<?php echo $menu->id ?>" class="custom-control-label text-success">Add Content Access</label>
                                    </div>

                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" name="edit_<?php echo $menu->id ?>" id="edit_<?php echo $menu->id ?>" value="1">
                                        <label for="edit_<?php echo $menu->id ?>" class="custom-control-label text-warning">Edit Content Access</label>
                                    </div>

                                    <div class="custom-control custom-checkbox">
                                        <input class="custom-control-input" type="checkbox" name="delete_<?php echo $menu->id ?>" id="delete_<?php echo $menu->id ?>" value="1">
                                        <label for="delete_<?php echo $menu->id ?>" class="custom-control-label text-danger">Delete Content Access</label>
                                    </div>
                                <?php endif; ?>
                            </div>
                            
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
            
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-success">Submit</button>
        </div>

        <?php echo form_close() ?>
    </div>
    <!-- /.card -->

</div>

<script>

    $(document).

</script>