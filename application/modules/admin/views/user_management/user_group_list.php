<!-- left column -->
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card">
        
        <div class="col-md-12 card-body" style="overflow-x: scroll;">

            <h4><?php echo $heading ?> <?php if (session_data('is_super_admin') == 1 || addAccess($dashboardMenuId) == 1): ?><a href="<?php echo base_url('admin-session/user-group-add') ?>" class="btn btn-success btn-sm float-right"><span class="fa fa-plus"></span> Add New</a><?php endif; ?></h4><hr>

            <?php if($this->session->flashdata('error_msg')): ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Sorry!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
                </div>
            <?php endif; ?>

            <?php if($this->session->flashdata('success_msg')): ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Success!</strong> <?php echo $this->session->flashdata('success_msg'); ?>
                </div>
            <?php endif; ?>

            <table class="table table-hover table-bordered" id="data_table_active">
                <thead>
                    <tr>
                        <th>S.N</th>
                        <th class="text-center">Action</th>
                        <th>Group Name</th>
                        <th>Description</th>
                        <th>Profile Update Access</th>
                        <th>Login Status</th>
                        <th>Status</th>
                    </tr>                                        
                </thead>
                    <?php if ($userGroupList): ?>
                        <?php foreach ($userGroupList as $key => $group):  ?>
                            <tr>
                                <td nowrap><?php echo ++$key.'.' ?></td>
                                <td class="text-center" nowrap>
                                    <?php if (session_data('is_super_admin') == 1 || editAccess($dashboardMenuId) == 1): ?>
                                        <a href="<?php echo base_url('admin-session/user-group-edit/'.$group->id) ?>" class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Edit"><span class="fa fa-edit" style="color: white"></span></a>&nbsp;   
                                    <?php endif; ?>

                                    <?php if (session_data('is_super_admin') == 1 || deleteAccess($dashboardMenuId) == 1): ?>
                                        <a href="<?php echo base_url('admin-session/user_group_delete/'.$group->id) ?>" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Delete" onClick="return confirm('Are you sure to permanently delete this data?')" ><span class="fa fa-trash" style="color: white"></span></a>  
                                    <?php endif; ?>

                                    <?php if (session_data('is_super_admin') == 0 && (deleteAccess($dashboardMenuId) == 0 && editAccess($dashboardMenuId) == 0)): ?>
                                        <span>-</span>
                                    <?php endif; ?>
                                </td>
                                <td nowrap><?php echo $group->group_name ?></td>
                                <td nowrap><?php echo $group->description ?></td>
                                <td nowrap class="text-center"><?php echo ($group->profile_update_access == 1) ? '<span class="badge badge-success">Authorized</span>' : '<span class="badge badge-danger">Not Authorized</span>' ?></td>
                                <td nowrap class="text-center"><?php echo ($group->login_status == 1) ? '<span class="badge badge-success">Authorized</span>' : '<span class="badge badge-danger">Not Authorized</span>' ?></td>
                                <td nowrap class="text-center"><?php echo ($group->status == 1) ? '<span class="badge badge-success">Active</span>' : '<span class="badge badge-danger">Inactive</span>' ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr class="text-center">
                            <td colspan='6'>No data available.</td>
                        </tr>
                    <?php endif; ?>
                <tbody>

                </tbody>
            </table>
            
            
        </div>
        <!-- /.card-body -->

        <!-- <div class="card-footer">
            <button type="submit" class="btn btn-success">Update Profile</button>
        </div> -->

    </div>
    <!-- /.card -->

</div>