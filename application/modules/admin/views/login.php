<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Admin | Login</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!-- icheck bootstrap -->
	<link rel="stylesheet" href="<?php echo base_url(ADMIN_ASSETS.'plugins/icheck-bootstrap/icheck-bootstrap.min.css') ?>">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url(ADMIN_ASSETS.'dist/css/adminlte.min.css') ?>">
	<!-- Google Font: Source Sans Pro -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition login-page">

	<div class="login-box">
		<div class="login-logo">
			<a href="#"><b>Project</b> Name </a>
		</div>

		<?php if($this->session->flashdata('error_msg')): ?>
			<div class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<strong>Sorry!</strong> <?php echo $this->session->flashdata('error_msg'); ?>
			</div>
		<?php endif; ?>

		<?php if($this->session->flashdata('success_msg')): ?>
			<div class="alert alert-success alert-dismissible">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				<strong>Success!</strong> <?php echo $this->session->flashdata('success_msg'); ?>
			</div>
		<?php endif; ?>


		<!-- /.login-logo -->
		<div class="card">
			<div class="card-body login-card-body">
				<p class="login-box-msg">Sign in to start your session</p>

	
				<?php
					$action = base_url('admin-session/login');
					$attributes = array(
						"id"        => "admin_login_form", 
						"name"      => "admin_login_form",
						"method"    => "POST"
					);

					echo form_open($action, $attributes); 
				?>

				<div class="input-group">
					<input type="text" class="form-control" placeholder="Username" name="username" value="<?php echo set_value('username') ?>" required>
					<div class="input-group-append input-group-text">
						<span class="fas fa-user"></span>
					</div>
				</div>
				<?php echo form_error('username'); ?><br>

				<div class="input-group">
					<input type="password" class="form-control" placeholder="Password" name="password" value="<?php echo set_value('password') ?>" required>
					<div class="input-group-append input-group-text">
						<span class="fas fa-lock"></span>
					</div>
				</div>
				<?php echo form_error('password'); ?><br>

				<div class="row">
					<div class="col-8">
						<div class="icheck-primary">
							<input type="checkbox" id="remember">
							<label for="remember">
								Remember Me
							</label>
						</div>
					</div>
					<!-- /.col -->

					<div class="col-4">
						<button type="submit" class="btn btn-primary btn-block btn-flat" name="login_btn">Sign In</button>
					</div>
					<!-- /.col -->
				</div>

				<?php echo form_close(); ?>

				<!-- <p class="mb-1">
					<a href="#">I forgot my password</a>
				</p>
				<p class="mb-0">
					<a href="register.html" class="text-center">Register a new membership</a>
				</p> -->
			</div>
			<!-- /.login-card-body -->
		</div>
	</div>
	<!-- /.login-box -->

</body>
</html>

<!-- jQuery -->
<script src="<?php echo base_url(ADMIN_ASSETS.'plugins/jquery/jquery.min.js') ?>"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url(ADMIN_ASSETS.'plugins/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>


<script>

    $(document).ready(function() {
        $(".alert-success").fadeTo(3500, 500).slideUp(500, function(){
            $(".alert-success").slideUp(300);
        });

        $(".alert-danger").fadeTo(3500, 500).slideUp(500, function(){
            $(".alert-danger").slideUp(300);
        });

        $(".alert-info").fadeTo(3500, 500).slideUp(500, function(){
            $(".alert-info").slideUp(300);
        });

        $(".alert-warning").fadeTo(3500, 500).slideUp(500, function(){
            $(".alert-warning").slideUp(300);
        });
    });

</script> 
