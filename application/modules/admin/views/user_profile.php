<!-- left column -->
<div class="col-md-12">
    <!-- general form elements -->
    <div class="card">
        
        <div class="col-md-12 card-body">
            <table class="table table-borderless" id="user_profile_table">
                <tr>
                    <th>First Name</th>
                    <td><?php echo ucfirst($user->firstname); ?></td>
                    <?php if ($userGroup->profile_update_access == 1): ?>
                        <td id="edit_user_profile_btn"><a class="btn btn-success btn-sm" href="<?php echo base_url("admin-session/user-profile-update") ?>">Update Profile</a></td>
                    <?php endif; ?>
                </tr>

                <tr>
                    <th>Last Name</th>
                    <td><?php echo ucfirst($user->lastname); ?></td>
                </tr>

                <tr>
                    <th>Date of Birth</th>
                    <td><?php echo ($user->dob) ? $user->dob : '-'; ?></td>
                </tr>

                <tr>
                    <th>Address</th>
                    <td><?php echo ($user->address) ? $user->address : '-'; ?></td>
                </tr>

                <tr>
                    <th>Gender</th>
                    <td><?php echo ($user->gender) ? ( ($user->gender == 'm') ? 'Male' : (($user->gender == 'f') ? 'Female' : 'Others') ) : '-'; ?></td>
                </tr>

                <tr>
                    <th>Contact</th>
                    <td><?php echo ($user->contact) ? $user->contact : '-'; ?></td>
                </tr>

                <tr>
                    <th>Username</th>
                    <td><?php echo $user->username ?></td>
                </tr>

                <tr>
                    <th>Email</th>
                    <td><?php echo $user->email ?></td>
                </tr>
            </table>
        </div>
        <!-- /.card-body -->

        
    </div>
    <!-- /.card -->

</div>