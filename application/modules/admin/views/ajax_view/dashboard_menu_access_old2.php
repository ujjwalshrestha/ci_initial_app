<!-- This page is loaded through ajax call. It is integrated in Manage Menu Access section and is loaded when both the select field i.e. Select User Group and Select Menu is changed. -->

<div class="col-md-2">
    <label>Select Content Access</label>
</div>
<div class="col-md-3">
    <div class="custom-control custom-checkbox">
        <input class="custom-control-input" type="checkbox" name="view" id="view" value="1" <?php if($menuAccess) echo ($menuAccess->view_access == 1) ? "checked" : ""; ?> >
        <label for="view" class="custom-control-label">View Content Access</label>
    </div>

    <?php if($menuDetail && $menuDetail->menu_type !== "p"): ?>
        <div class="custom-control custom-checkbox">
            <input class="custom-control-input" type="checkbox" name="add" id="add" value="1" <?php if($menuAccess) echo ($menuAccess->add_access == 1) ? "checked" : ""; ?> >
            <label for="add" class="custom-control-label">Add Content Access</label>
        </div>

        <div class="custom-control custom-checkbox">
            <input class="custom-control-input" type="checkbox" name="edit" id="edit" value="1" <?php if($menuAccess) echo ($menuAccess->edit_access == 1) ? "checked" : ""; ?> >
            <label for="edit" class="custom-control-label">Edit Content Access</label>
        </div>

        <div class="custom-control custom-checkbox">
            <input class="custom-control-input" type="checkbox" name="delete" id="delete" value="1" <?php if($menuAccess) echo ($menuAccess->delete_access == 1) ? "checked" : ""; ?> >
            <label for="delete" class="custom-control-label">Delete Content Access</label>
        </div>
    <?php endif; ?>
</div>


