<?php 

if( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
* 
*/
class Site extends MY_Controller # My_Contrller is in core directory.
{
    /*
	 *	variable to hold data for view
	 */
	private static $viewData = array();
	
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
        redirect('coming-soon'); // redirects to coming soon page
        exit;
        
		self::$viewData['title'] = 'Index';
        self::$viewData['page'] = 'site/home';
        $this->load->view(TEMPL_SITE, self::$viewData);
    }

    /**
     * This function returns the Coming-Soon view page.
     *
     * @return view
     */
    public function comingSoon()
	{
		self::$viewData['title'] = "Initial App | Coming Soon";
        $this->load->view('site/coming_soon', self::$viewData);
	}
    
    /**
     * This function is executed upon ajax request. Here, the procedure of email subscription is carried out. If the user is new to subscription, then their data is inserted with status 1 as default. If the user's data already exists, then their status is updated from 0 to 1 or will display message of being already subscribed.
     *
     * @return json
     */
	public function subscribe()
	{
		try {
			if ($this->input->is_ajax_request()) {
                $csrf_name = $this->security->get_csrf_token_name(); // get csrf name
                $csrf_value = $this->security->get_csrf_hash(); // get new csrf value

                $email = $this->input->post('email');
                // checks if the imput field is empty or not
				if (empty($email)) {
					$response = array(
						"status" => "error",
                        "message" => "Please enter your email address.",
                        "csrf_name" => $csrf_name,
                        "csrf_value" => $csrf_value
					);
					header("Content-type: application/json");
					echo json_encode($response); exit;
				}

                // checks if email format is valid or not
				if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
					$response = array(
						"status" => "error",
						"message" => "Please enter valid email address.",
                        "csrf_name" => $csrf_name,
                        "csrf_value" => $csrf_value
					);
					header("Content-type: application/json");
					echo json_encode($response); exit;
				}

				$userSubscription = $this->site_model->getUserSubscription($email);

				if ($userSubscription) { // condition where user had already subscribed before
					$status = $userSubscription->status;

					// If the subscription status is 1, then user is already subscribed
					if ($status == 1) {
						$response = array(
							"status" => "error",
							"message" => "You are already subscribed.",
                            "csrf_name" => $csrf_name,
                            "csrf_value" => $csrf_value
						);
						header("Content-type: application/json");
						echo json_encode($response); exit;

					// If the subscription status is 0, then user will be subscribed
					} elseif ($status == 0) {
                        $token = $userSubscription->token; // get token of the respective subscribed user 
                        
						$updateData = array(
							"status" => 1,
							"subscribed_at" => date('Y-m-d H:i:s')
						);

						$updateSubscription = $this->site_model->updateSubscription($updateData, $email, $token);

						if ($updateSubscription) {
                            // send mail to the subscribed user
                            $to = $email;
                            $from = "noreply.micheck@gmail.com";
                            $from_title = "CI Initial App";
                            $subject = "User Subscription Alert";

                            $message = "<a href='".base_url()."site/unsubscribe?e=".urlEncrypt($email)."&t=".$token."'>UNSUBSCRIBE</a>";

                            $send = $this->email_alert->sendMail($to, $from, $from_title, $subject, $message, null, null);
                            // send mail ends here

							$response = array(
                                "status" => "success",
                                "title" => "You Have Been Subscribed!",
                                "message" => "Thank you for the subscription. You will get notification on your e-mail regarding further updates.",
                                "csrf_name" => $csrf_name,
                                "csrf_value" => $csrf_value
                            );
							header("Content-type: application/json");
							echo json_encode($response); exit;

						} else {
							$response = array(
								"status" => "error",
								"message" => "Sorry! Something went wrong. Please try again later.",
                                "csrf_name" => $csrf_name,
                                "csrf_value" => $csrf_value
							);
							header("Content-type: application/json");
							echo json_encode($response); exit;
						}
					}

				} else { // condition where user is new to subscription
					$token = hashCode(); // a hashcode (token) is generated
					
					$insertData = array(
						"email" => $email,
						"token" => $token
					);
					$newSubscription = $this->site_model->newSubscription($insertData);

					if ($newSubscription) {
                        // send mail to the subscribed user
                        $to = $email;
                        $from = "noreply.micheck@gmail.com";
                        $from_title = "CI Initial App";
						$subject = "User Subscription Alert";

						$message = "<a href='".base_url()."site/unsubscribe?e=".urlEncrypt($email)."&t=".$token."'>UNSUBSCRIBE</a>";

                        $send = $this->email_alert->sendMail($to, $from, $from_title, $subject, $message, null, null);
                        // send mail ends here

						$response = array(
							"status" => "success",
							"title" => "You Have Been Subscribed!",
							"message" => "Thank you for the subscription. You will get notification on your e-mail regarding further updates.",
                            "csrf_name" => $csrf_name,
                            "csrf_value" => $csrf_value
						);
						header("Content-type: application/json");
						echo json_encode($response); exit;

					} else {
						$response = array(
							"status" => "error",
							"message" => "Unable to subscribe. Please try again later.",
                            "csrf_name" => $csrf_name,
                            "csrf_value" => $csrf_value
						);
						header("Content-type: application/json");
						echo json_encode($response); exit;
					}
				}

			} else {
				exit("No direct script allowed!");
			}

		} catch (Exception $e) {
			echo "Caught exception: ". $e->getMessage();
		}
	}
    
    /**
     * This function is executed upon ajax request. Here, through this function, user is unsubscribed.
     *
     * @return view
     */
    public function unsubscribe()
    {
        $email = urlDecrypt($this->input->get('e')); // decrypting the encrypted email
        $token = $this->input->get('t');

        $unsubscribe = $this->site_model->unsubscribe($email, $token);

        if ($unsubscribe) {
            self::$viewData['email'] = $email;
            self::$viewData['token'] = $token;
            self::$viewData['title'] = "Initial App | Unsubscribed";

            $this->load->view('site/unsubscribed_page', self::$viewData);
        } else {
            echo "Internal Server Error!";
        }
    }

    /**
     * This Through this function, user is re-subcribed. 
     *
     * @return json
     */
    public function resubscribe()
    {
        try {
			if ($this->input->is_ajax_request()) {
                $email = urlDecrypt($this->input->get('e'));
                $token = $this->input->get('t');

                $updateData = array(
                    "status" => 1,
                    "subscribed_at" => date('Y-m-d H:i:s')
                );

                $updateSubscription = $this->site_model->updateSubscription($updateData, $email, $token);

                if ($updateSubscription) {
                    // send mail to the subscribed user
                    $to = $email;
                    $from = "noreply.micheck@gmail.com";
                    $from_title = "CI Initial App";
                    $subject = "User Subscription Alert";

                    $message = "Welcome Back! <br> <a href='".base_url()."site/unsubscribe?e=".urlEncrypt($email)."&t=".$token."'>UNSUBSCRIBE</a>";

                    $send = $this->email_alert->sendMail($to, $from, $from_title, $subject, $message, null, null);
                    // send mail ends here

                    $response = array(
                        "status" => "success",
                        "title" => "You Have Been Re-Subscribed!",
                        "message" => "Thank you for the subscription. You will get notification on your e-mail regarding further updates."
                    );
                    header("Content-type: application/json");
                    echo json_encode($response); exit;

                } else {
                    $response = array(
                        "status" => "error",
                        "message" => "Sorry! Something went wrong. Please try again later."
                    );
                    header("Content-type: application/json");
                    echo json_encode($response); exit;
                }

            } else {
				exit("No direct script allowed!");
			}

		} catch (Exception $e) {
			echo "Caught exception: ". $e->getMessage();
		}
    }
}