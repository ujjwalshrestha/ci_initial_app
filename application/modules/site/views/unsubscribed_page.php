<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(SITE_ASSETS.'coming_soon/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(SITE_ASSETS.'coming_soon/css/fontawesome-all.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(SITE_ASSETS.'coming_soon/css/iosoon-style.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(SITE_ASSETS.'coming_soon/css/iosoon-theme8.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(SITE_ASSETS.'sweetalert/sweetalert_custom.css') ?>">
</head>

<body>
    <div class="form-body no-side">
        <canvas id="pagebg" resize></canvas>
        <div class="row">
            <div class="form-holder">
                <div class="form-content">
                    <div class=""><br>
                        <div class="website-logo-inside">
                            <a href="index.html">
                                <div class="logo">
                                <img class="logo-size" src="<?php echo base_url(SITE_ASSETS.'coming_soon/images/logo-light.svg') ?>" alt="">
                                </div>
                            </a>
                        </div><br>
                        <h1>You Have Been Unsubscribed!</h1>

                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <p>So we're emailing you one last time we can take a hint. Don't want to miss out a New
                                    update or Let us know if you want to continue receiving emails from us,
                                    click button below.
                                </p>
                            </div>
                            <div class="col-md-2"></div>
                        </div>

                        <div class="col-md-12">
                            <a href="<?php echo base_url('site/resubscribe?e='.urlEncrypt($email).'&t='.$token) ?>" type="button" id="resubscribe_btn" class="btn btn-success btn-lg">RE-SUBSCRIBE</a>
                        </div>

                        <div class="spacer"></div>
                        <div class="spacer"></div>
                        <div class="spacer"></div>

                        <div class="other-links no-bg-icon">
                            <a href="#" title="Facebook"><i class="fab fa-facebook-f"></i></a>
                            <a href="#" title="Twitter"><i class="fab fa-twitter"></i></a>
                            <a href="#" title="LinkedIn"><i class="fab fa-linkedin-in"></i></a>
                            <a href="#" title="Instagram"><i class="fab fa-instagram"></i></a>
                        </div>

                        <div class="spacer"></div>
                        <div class="spacer"></div>

                        <!-- <a href="<?php echo base_url(); ?>">Go to Site</a> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="<?php echo base_url(SITE_ASSETS.'coming_soon/js/jquery.min.js') ?>"></script>
<script src="<?php echo base_url(SITE_ASSETS.'coming_soon/js/popper.min.js') ?>"></script>
<script src="<?php echo base_url(SITE_ASSETS.'coming_soon/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url(SITE_ASSETS.'coming_soon/js/paper-full.min.js') ?>"></script>
<script src="<?php echo base_url(SITE_ASSETS.'coming_soon/js/animation6.js') ?>"></script>
<script src="<?php echo base_url(SITE_ASSETS.'coming_soon/js/main.js') ?>"></script>
<script src="<?php echo base_url(SITE_ASSETS.'sweetalert/sweetalert.min.js') ?>"></script>

<script>
    /**
     * When 'Re-Subscribe' button is clicked, the pointer-event (mouse pointer) on the button will be disabled until the completion of ajax response.
     */
    $(function(){
        var obj = $("#resubscribe_btn");

        $(document).ajaxStart(function(){
            obj.css("pointer-events", "none");
        });

        $(document).ajaxComplete(function(){
            obj.css("pointer-events", "");
        }); 
    });

    /**
     * On clicking re-subscribe button, click event is triggered on the button element. Here, 'get' method is used for passing data through ajax. The 'email' and 'token' is passed through href attribute, which is then acquired as GET in controller.
     */
    $(document).ready(function(){
        $(document).off('click', '#resubscribe_btn').on('click', '#resubscribe_btn', function(e){
            e.preventDefault();
            var obj = $(this),
                url = obj.attr("href"); // gets the url from href attribute of anchor tag.

            $.ajax({
                type : "get",
                url  : url,
                dataType : 'json',
                success:function(resp){
                    if (resp.status == 'error') { // if the response status is error
                        swal(resp.message, "", "warning");
                    } else if (resp.status == 'success') { // if the response status is success
                        swal(resp.title, resp.message, 'success')
                        .then((value) => { // on clicking OK or outside the alert box, it will redirect to landing page of the site
                            if (value == true || value == null) {
						        window.location = "<?php echo base_url(); ?>";
                            }
                        });
                    }
                }, error: function(){
                    swal("Error!", "Internal Server Error", "error");
                }
            });
        });
    });
</script>

</body>
</html>