<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(SITE_ASSETS.'coming_soon/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(SITE_ASSETS.'coming_soon/css/fontawesome-all.min.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(SITE_ASSETS.'coming_soon/css/iosoon-style.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(SITE_ASSETS.'coming_soon/css/iosoon-theme8.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(SITE_ASSETS.'sweetalert/sweetalert_custom.css') ?>">
</head>
<body>
    <div class="form-body no-side">
        <canvas id="pagebg" resize></canvas>
        <div class="row">
            <div class="form-holder">
                <div class="form-content">
                    <div class="form-items"><br>
                        <div class="website-logo-inside">
                            <a href="index.html">
                                <div class="logo">
                                    <img class="logo-size" src="<?php echo base_url(SITE_ASSETS.'coming_soon/images/logo-light.svg') ?>" alt="">
                                </div>
                            </a>
                        </div><br>
                        <h1>We’re Coming Soon!</h1>
                        <p>Our website is under constructions currently. Get a notification on your e-mail for updates.</p>

                        <div class="spacer"></div>

                        <?php
                            $action = base_url('site/subscribe');
                            $attributes = array(
                                "class" => "form-row",
                                "id" => "subscribe_form", 
                                "name" => "subscribe_form",
                                "method" => "POST"
                            );
                            echo form_open($action, $attributes); 
                        ?>
                            <div class="col-md-8">
                                <input type="text" class="form-control" placeholder="E-mail Address" name="email">
                            </div>
                            <div class="col-md-4">
                                <button type="submit" class="btn btn-success" id="submit_btn" name="submit">SUBSCRIBE</button>
                            </div>
                        <?php echo form_close() ?>

                        <div class="spacer"></div>
                        
                        <div class="spacer"></div>
                        <div class="other-links no-bg-icon">
                            <a href="#" title="Facebook"><i class="fab fa-facebook-f"></i></a>
                            <a href="#" title="Twitter"><i class="fab fa-twitter"></i></a>
                            <a href="#" title="LinkedIn"><i class="fab fa-linkedin-in"></i></a>
                            <a href="#" title="Instagram"><i class="fab fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="<?php echo base_url(SITE_ASSETS.'coming_soon/js/jquery.min.js') ?>"></script>
<script src="<?php echo base_url(SITE_ASSETS.'coming_soon/js/popper.min.js') ?>"></script>
<script src="<?php echo base_url(SITE_ASSETS.'coming_soon/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url(SITE_ASSETS.'coming_soon/js/paper-full.min.js') ?>"></script>
<script src="<?php echo base_url(SITE_ASSETS.'coming_soon/js/animation6.js') ?>"></script>
<script src="<?php echo base_url(SITE_ASSETS.'coming_soon/js/main.js') ?>"></script>
<script src="<?php echo base_url(SITE_ASSETS.'sweetalert/sweetalert.min.js') ?>"></script>

<script>
    /**
     * When 'Subscribe' button is clicked, the button will be disabled until the completion of ajax response.
     */
    $(function(){
        $(document).ajaxStart(function(){
            $("#submit_btn").attr("disabled", true);
        });

        $(document).ajaxComplete(function(){
            $("#submit_btn").attr("disabled", false);
        }); 
    });

    /**
     * On submitting form, submit event is triggered on the form.
     */
    $(document).ready(function(){
        $(document).off('submit', '#subscribe_form').on('submit', '#subscribe_form', function(e){
            e.preventDefault();
            var obj = $(this), // refers to the element ('#subscribe_form') on which the event ('submit') is applied
                data = obj.serialize(), // gets all the submitted data from form
                url = obj.attr("action"); // gets the url action of the form

            $.ajax({
                type : "post",
                url : url,
                data : data,
                dataType : 'json',
                success:function(resp){
                    $("input[name="+resp.csrf_name+"]").val(resp.csrf_value); // replaces the csrf value from the hidden input field with the new one.

                    if (resp.status == 'error') { // if the response status is error
                        swal(resp.message, "", "warning");
                    } else if (resp.status == 'success') { // if the response status is success
                        swal(resp.title, resp.message, 'success');
                    }
                }, error: function(){
                    swal("Error!", "Internal Server Error", "error");
                }
            });
        });
    });
</script>

</body>
</html>

