<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
*
*/
class Site_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
    }

	/**
	 * This function returns the data of user through its email address.
	 *
	 * @param string $email
	 * @return array
	 */
	public function getUserSubscription($email)
	{
		$where = array("email" => $email);
		$result = $this->db->get_where("user_subscription", $where);

		if ($result->num_rows() == 1) {
			return $result->row();
		} else {
			return false;
		}
    }
	
	/**
	 * This function inserts the data of new subscriber.
	 *
	 * @param array $insertData
	 * @return boolean
	 */
    public function newSubscription($insertData)
    {
        $this->db->insert("user_subscription", $insertData);
        
		if ($this->db->affected_rows() == 1) {
			return true;
		} else {
			return false;
		}
    }

	/**
	 * This function updates the subscription status of user from 0 to 1.
	 *
	 * @param array $updateData
	 * @param string $email
	 * @param string $token
	 * @return boolean
	 */
    public function updateSubscription($updateData, $email, $token)
    {
		$where = array(
			"email" => $email,
			"token" => $token
		);

		$this->db->where($where);
		$this->db->update('user_subscription', $updateData);

		if ($this->db->affected_rows() == 1) {
            return true;
        } else {
            return false;
        }
	}
	
	/**
	 * This function updates the subscription status of user from 1 to 0.
	 *
	 * @param string $email
	 * @param string $token
	 * @return boolean
	 */
	public function unsubscribe($email, $token)
	{
		$where = array(
			"email" => $email,
			"token" => $token
		);

		$updateData = array(
			"status" => 0,
			"unsubscribed_at" => date('Y-m-d H:i:s')
		);

		$this->db->where($where);
		$this->db->update('user_subscription', $updateData);

		if ($this->db->affected_rows() == 1) {
            return true;
        } else {
            return false;
        }
	}
}

    