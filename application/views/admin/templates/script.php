
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url(ADMIN_ASSETS.'plugins/jquery-ui/jquery-ui.min.js') ?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url(ADMIN_ASSETS.'plugins/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>
<!-- ChartJS -->
<script src="<?php echo base_url(ADMIN_ASSETS.'plugins/chart.js/Chart.min.js') ?>"></script>
<!-- Sparkline -->
<script src="<?php echo base_url(ADMIN_ASSETS.'plugins/sparklines/sparkline.js') ?>"></script>
<!-- JQVMap -->
<script src="<?php echo base_url(ADMIN_ASSETS.'plugins/jqvmap/jquery.vmap.min.js') ?>"></script>
<script src="<?php echo base_url(ADMIN_ASSETS.'plugins/jqvmap/maps/jquery.vmap.world.js') ?>"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url(ADMIN_ASSETS.'plugins/jquery-knob/jquery.knob.min.js') ?>"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url(ADMIN_ASSETS.'plugins/moment/moment.min.js') ?>"></script>
<script src="<?php echo base_url(ADMIN_ASSETS.'plugins/daterangepicker/daterangepicker.js') ?>"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?php echo base_url(ADMIN_ASSETS.'plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') ?>"></script>
<!-- Summernote -->
<script src="<?php echo base_url(ADMIN_ASSETS.'plugins/summernote/summernote-bs4.min.js') ?>"></script>
<!-- overlayScrollbars -->
<script src="<?php echo base_url(ADMIN_ASSETS.'plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') ?>"></script>
<!-- FastClick -->
<script src="<?php echo base_url(ADMIN_ASSETS.'plugins/fastclick/fastclick.js') ?>"></script>
<!-- DataTables -->
<script src="<?php echo base_url(ADMIN_ASSETS.'plugins/datatables/jquery.dataTables.min.js') ?>"></script>
<script src="<?php echo base_url(ADMIN_ASSETS.'plugins/datatables/dataTables.bootstrap4.min.js') ?>"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(ADMIN_ASSETS.'dist/js/adminlte.js') ?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo base_url(ADMIN_ASSETS.'dist/js/pages/dashboard.js') ?>"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(ADMIN_ASSETS.'dist/js/demo.js') ?>"></script>


<script>

    $(document).ready(function() {
        // Alert message div will disappear itself after probably 5 sec.
        $(".alert-success").fadeTo(2500, 500).slideUp(500, function(){
            $(".alert-success").slideUp(300);
        });

        // Alert message div will disappear itself after probably 5 sec.
        $(".alert-danger").fadeTo(2500, 500).slideUp(500, function(){
            $(".alert-danger").slideUp(300);
        });

        // Alert message div will disappear itself after probably 5 sec.
        $(".alert-info").fadeTo(2500, 500).slideUp(500, function(){
            $(".alert-info").slideUp(300);
        });

        // Alert message div will disappear itself after probably 5 sec.
        $(".alert-warning").fadeTo(2500, 500).slideUp(500, function(){
            $(".alert-warning").slideUp(300);
        });

        // Boostrap tooltip
        $('[data-toggle="tooltip"]').tooltip();

        // DataTable
        $('#data_table_active').DataTable();
    });


    // For generating slug automatically. Here, the slug with be generated on the field named 'slugged' from the field named 'name' and 'title'.
    $(document).off('keyup', 'input[name="name"], input[name="title"]').on('keyup', 'input[name="name"], input[name="title"]', function(e){
        e.preventDefault();
        var obj = $(this);
        var str = obj.val();
        var trims = $.trim(str);
        var slug = trims.replace(/[^a-z0-9]/gi, '-').replace(/^-|-$/g, '');
        $('input[name="slug"]').val(slug.toLowerCase())
    });

</script>        