<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
        <img src="<?php echo base_url(ADMIN_ASSETS.'dist/img/AdminLTELogo.png') ?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">Administrator</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <!-- <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="<?php //echo base_url(ADMIN_ASSETS.'dist/img/user2-160x160.jpg') ?>" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">Alexander Pierce</a>
            </div>
        </div> -->

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class with font-awesome or any other icon font library -->

                <!-- This section of dashboard menu sidebar is only for super admin -->
                <?php if (session_data('is_super_admin') == 1): ?>
                
                    <!-- checking if there is dashboard side menu or not -->
                    <?php if ($dashboardSideMenu): ?>
                        <!-- dashboard side menu loop -->
                        <?php foreach ($dashboardSideMenu as $key => $menu): ?>
                            <!-- checking if the menu is parent menu -->
                            <?php if ($menu->menu_type == 'p'): ?>

                                <li class="nav-item has-treeview">
                                    <a href="#" class="nav-link">
                                        <i class="nav-icon <?php echo $menu->menu_icon ?>"></i>
                                        <p>
                                            <?php echo $menu->menu_name ?>
                                            <i class="fas fa-angle-left right"></i>
                                            <!-- <span class="badge badge-info right">6</span> -->
                                        </p>
                                    </a>
                                    <?php
                                        // child menus of parent menu
                                        $child_menu = dashboardChildMenu($menu->id);
                                        if ($child_menu):
                                    ?>
                                        <ul class="nav nav-treeview">
                                            <!-- child menu listing through loop -->
                                            <?php foreach ($child_menu as $key => $menu): ?>
                                                <li class="nav-item">
                                                    <a href="<?php echo base_url('admin-session/'.$menu->slug) ?>" class="nav-link">
                                                        <i class="<?php echo $menu->menu_icon ?> nav-icon text-danger"></i>
                                                        <p><?php echo $menu->menu_name ?></p>
                                                    </a>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    <?php endif; ?>
                                </li>

                            <!-- if menu is not parent -->
                            <?php else: ?>

                                <li class="nav-item">
                                    <a href="<?php echo base_url('admin-session/'.$menu->slug) ?>" class="nav-link">
                                        <i class="nav-icon <?php echo $menu->menu_icon ?>"></i>
                                        <p class="text"><?php echo $menu->menu_name ?></p>
                                    </a>
                                </li>

                            <?php endif; ?>

                        <?php endforeach; ?>
                    <?php endif; ?>
            
                <?php endif; ?>
                <!-- // This section of dashboard menu sidebar is only for super admin ends here -->


                <!-- This section of dashboard menu sidebar is for other users except super admin -->
                <?php if (session_data('is_super_admin') == 0): ?>
                
                    <!-- checking if there is dashboard side menu or not -->
                    <?php if ($dashboardSideMenu): ?>
                        <!-- dashboard side menu loop -->
                        <?php foreach ($dashboardSideMenu as $key => $menu): ?>
                            <!-- checking if the menu is parent menu -->
                            <?php if ($menu->menu_type == 'p'): ?>
                                <?php if (viewAccess($menu->id) == 1): ?>
                                    <li class="nav-item has-treeview">
                                        <a href="#" class="nav-link">
                                            <i class="nav-icon <?php echo $menu->menu_icon ?>"></i>
                                            <p>
                                                <?php echo $menu->menu_name ?>
                                                <i class="fas fa-angle-left right"></i>
                                                <!-- <span class="badge badge-info right">6</span> -->
                                            </p>
                                        </a>
                                        <?php
                                            // child menus of parent menu
                                            $child_menu = dashboardChildMenu($menu->id);
                                            if ($child_menu):
                                        ?>
                                            <ul class="nav nav-treeview">
                                                <!-- child menu listing through loop -->
                                                <?php foreach ($child_menu as $key => $menu): ?>
                                                    <?php if (viewAccess($menu->id) == 1): ?>
                                                        <li class="nav-item">
                                                            <a href="<?php echo base_url('admin-session/'.$menu->slug) ?>" class="nav-link">
                                                                <i class="<?php echo $menu->menu_icon ?> nav-icon text-danger"></i>
                                                                <p><?php echo $menu->menu_name ?></p>
                                                            </a>
                                                        </li>
                                                    <?php endif; ?>
                                                <?php endforeach; ?>
                                            </ul>
                                        <?php endif; ?>
                                    </li>
                                <?php endif; ?>
                            <!-- if menu is not parent -->
                            <?php else: ?>
                                <?php if (viewAccess($menu->id) == 1): ?>
                                    <li class="nav-item">
                                        <a href="<?php echo base_url('admin-session/'.$menu->slug) ?>" class="nav-link">
                                            <i class="nav-icon <?php echo $menu->menu_icon ?>"></i>
                                            <p class="text"><?php echo $menu->menu_name ?></p>
                                        </a>
                                    </li>
                                <?php endif; ?>

                            <?php endif; ?>

                        <?php endforeach; ?>
                    <?php endif; ?>
            
                <?php endif; ?>
                <!-- // This section of dashboard menu sidebar is only for super admin ends here -->


                <!-- <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-copy"></i>
                        <p>
                            Tree
                            <i class="fas fa-angle-left right"></i>
                            <span class="badge badge-info right">6</span>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Subtree-1</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Subtree-2</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Subtree-3</p>
                            </a>
                        </li>
                    </ul>
                </li>
                
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon far fa-circle text-danger"></i>
                        <p class="text">Important</p>
                    </a>
                </li>
                
                
                <li class="nav-header">LABELS</li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon far fa-circle text-danger"></i>
                        <p class="text">Important</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon far fa-circle text-warning"></i>
                        <p>Warning</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                        <i class="nav-icon far fa-circle text-info"></i>
                        <p>Informational</p>
                    </a>
                </li> -->
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>