<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This model is for user authentication purpose.
 */
class Auth_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
    }

	/**
	 * Through this function, login process is carried out. From the username, overall session detail of the respective user is fetched. Then the login status of user group of respective username is checked. Then the password is checked, if matched or not. After that, user status is checked, if active or not. After completion of these three procedure, session data is set of the respective user.
	 *
	 * @param string $username
	 * @param string $password
	 * @return void
	 */
    public function authLogin($username, $password)
	{
		$where = array(
			'username' => $username
		);

		// getting user's detail through the username
        $result = $this->db->get_where('auth_user', $where);

		if ($result->num_rows() == 1) {
			$detail = $result->row();
			$user_id = $detail->id; // get user id
			$userGroup = $this->getUserGroupById($user_id); // get user-group detail by userid

			if ($userGroup->login_status == 1) { // check login status of user group
				$decryptedPassword = decrypt_password($detail->password); // decrypt user's password stored in database

				// Check if the password entered by user and the decrypted password matches or not.
				if ($password == $decryptedPassword) {

					// Check if the user status is active or not
					if ( ($detail->temp_status == 1) && ($detail->active_status == 1) ) {
						$group_id = $userGroup->group_id;
						$group_name = $userGroup->group_name;
						
						// Updates IP Address and Last login datetime of the current logged in user.
						$this->updateIpAddrLastLogin($user_id); 

						// declaring required session data after login
						$session_data = array(
							'user_id' => $user_id, // setting userid in session
							'username' => $username, // setting username in session
							'group_id' => $group_id, // setting groupid in session
							'group_name' => $group_name, // setting groupname in session
							'is_super_admin' => $detail->is_super_admin, // setting is_super_admin in session
						);
						$this->session->set_userdata($session_data); // setting data in session
						return 'success';

					} else {
						return '403'; 
					}

				} else {
					return false;
				}
			} else {
				return '403'; 
			}

        } else {
            return false;
        }
	}	


	/**
	 * During the login process, IP Address and last login date and time of the respective logged in user is updated through this function.
	 *
	 * @param int $user_id
	 * @return boolean
	 */
	public function updateIpAddrLastLogin($user_id)
	{
		$update_data = array(
			'ip_address' => getUserIpAddr(),
			'last_login' =>	date('Y-m-d H:i:s')	
		);

		$where = array(
			'id' => $user_id
		);

		$this->db->where($where);
		return $this->db->update('auth_user', $update_data);
    }


	/**
	 * Through this function, group ID and and group name of the respective logged in user is fetched through its user id.
	 *
	 * @param int $user_id
	 * @return void
	 */
	public function getUserGroupById($user_id)
	{
		$where = array(
			'ugm.user_id' => $user_id
		);

		$result = $this->db->select('ag.id as group_id, ag.group_name as group_name, ag.login_status')
						   ->from('auth_user_group_mapping as ugm')
						   ->join('auth_group as ag', 'ugm.group_id = ag.id')
						   ->where($where)
						   ->get();
		
		if ($result->num_rows() == 1) {
			return $result->row();

		} else {
			return false;
		}
	}
	
	
	/**
	 * Through this function, all the user session is unset and destroyed.
	 *
	 * @param string $redirect_path
	 * @return void
	 */
    public function authLogout($redirect_path)
    {	
		// Checks if not logged in 
		if (!isLoggedin()) {
			redirect($redirect_path, 'refresh');
			exit;
		}
		
        $unset = array('user_id', 'user_name', 'group_id', 'group_name', 'is_super_admin');
		$this->session->unset_userdata($unset); // unset login session data
        $this->session->set_flashdata('success_msg', 'Logged Out.'); // set success message
		redirect($redirect_path);
        $this->session->sess_destroy(); // destroy whole session
    }


	/**
	 * Through this function, main user detail from table 'tbl_users_main' is fetched.
	 *
	 * @return void
	 */
	public function getAuthUser()
	{
		$user_id = session_data('user_id'); // userid from session

		$where = array(
			'id' => $user_id
		);

		$result = $this->db->get_where('auth_user', $where);

		if ($result->num_rows() == 1) {
			return $result->row();

		} else {
			return false;
		}
	}


	/**
	 * Through this funtion, new password is updated.
	 *
	 * @param string $password_encrypted Ecrypted password.
	 * @return void
	 */
	public function updatePassword($password_encrypted)
	{
		$user_id = session_data('user_id');

		$where = array(
			'id' => $user_id
		);

		$update_data = array(
			'password' => $password_encrypted,
			'updated_by' => session_data('user_id'),
			'updated_at' => date('Y-m-d H:i:s')
		);

		$this->db->where($where);
		$this->db->update('auth_user', $update_data);

		if ($this->db->affected_rows() == 1) {
            return true;
            
        } else {
            return false;
        }
	}
}