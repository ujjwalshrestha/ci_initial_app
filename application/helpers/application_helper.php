<?php 

if( ! defined('BASEPATH')) exit('No direct script access allowed');


if ( !function_exists('moneyFormat') ) {
    /**
     * This function returns the money currency in Nepali format.
     *
     * @param float $num
     * @return currency
     */
	function moneyFormat($num) 
	{
		$explrestunits = "" ;
	
		if(strrchr($num, ".")) {
			$dec = strrchr($num, ".");
			$int = $num - $dec;
	
			if(strlen($int) > 3) {
				$num = $num - $dec;
				$lastthree = substr($num, strlen($num)-3, strlen($num));
				$restunits = substr($num, 0, strlen($num)-3); // extracts the last three digits
				$restunits = (strlen($restunits)%2 == 1)?"0".$restunits:$restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
				$expunit = str_split($restunits, 2);

				for($i=0; $i < sizeof($expunit); $i++) {
					// creates each of the 2's group and adds a comma to the end
					if($i == 0) {
						$explrestunits .= (int)$expunit[$i].","; // if is first value , convert into integer
					} else {
						$explrestunits .= $expunit[$i].",";
					}
				}
				$thecash = $explrestunits.$lastthree.$dec;

			} else {
				$thecash = $num;
			}
		
		} else {
			if(strlen($num) > 3) {
				$lastthree = substr($num, strlen($num)-3, strlen($num));
				$restunits = substr($num, 0, strlen($num)-3); // extracts the last three digits
				$restunits = (strlen($restunits) % 2 == 1) ? "0".$restunits : $restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
				$expunit = str_split($restunits, 2);
				
				for($i=0; $i < sizeof($expunit); $i++) {
					// creates each of the 2's group and adds a comma to the end
					if($i == 0) {
						$explrestunits .= (int)$expunit[$i].","; // if is first value , convert into integer
					} else {
						$explrestunits .= $expunit[$i].",";
					}
				}
				$thecash = $explrestunits.$lastthree;

			} else {
				$thecash = $num;
			}
		}
		return $thecash; // writes the final format where $currency is the currency symbol.
	}
}

if ( !function_exists('time_since') ) {
    /**
     * This functions returns the period according to date and time fetched.
     *
     * @param datetime $since
     * @return period
     */
	function time_since($since) 
	{
		$chunks = array(
			array(60 * 60 * 24 * 365 , 'year'),
			array(60 * 60 * 24 * 30 , 'month'),
			array(60 * 60 * 24 * 7, 'week'),
			array(60 * 60 * 24 , 'day'),
			array(60 * 60 , 'hr'),
			array(60 , 'min'),
			array(1 , 'sec')
		);

		for ($i = 0, $j = count($chunks); $i < $j; $i++) {
			$seconds = $chunks[$i][0];
			$name = $chunks[$i][1];
			if (($count = floor($since / $seconds)) != 0) {
				break;
			}
		}

		$print = ($count == 1) ? '1 '.$name : "$count {$name}s";
		return $print;
	} 
}

if ( !function_exists('remaining_days') ) {
    /**
     * This function returns the remanining days according to the date fetched.
     *
     * @param date $eventdate
     * @return string
     */
	function remaining_days($eventdate)
	{
		$date = date('Y-m-d');
		$date = strtotime($date);
		$datediff = $eventdate - $date;
		$status = floor($datediff / (60 * 60 * 24));

		if($status == 0){
			$message = "Today is the event day.";

		} elseif($status > 0) {
			$message = $status." day/s to go.";

		} else {
			$message = "Event finished";
		}

		return $message;
	}
}


if ( !function_exists('getUserIpAddr') ) {
	/**
	 * This function returns the IP Address of the user.  
	 *
	 * @return string
	 */
	function getUserIpAddr()
	{
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			//ip from share internet
			$ip = $_SERVER['HTTP_CLIENT_IP'];

		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			//ip pass from proxy
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];

		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		
		return $ip;
	}
}

if ( !function_exists('_session') ) {
	/**
	 * Self customised short way for executing session data. 
	 *
	 * @param string $data
	 * @return array
	 */
	function session_data($data = NULL)
	{
		$CI =& get_instance();
		return $CI->session->userdata($data);
	}
}


if ( !function_exists('uri_segment') ) {
	/**
	 * Self customised short way for executing uri segment. 
	 *
	 * @param int $segment
	 * @return string
	 */
	function uri_segment($segment = NULL)
	{
		$CI =& get_instance();
		return $CI->uri->segment($segment);
	}
}


if ( !function_exists('isLoggedin') ) {
	/**
	 * Checks whether the user is logged in or not.
	 *
	 * @return boolean
	 */
	function isLoggedin()
	{
		$CI =& get_instance();
		
		if ($CI->session->userdata('user_id') != '') {
			return true;
		} else {
			return false;
		}
	}
}


if ( !function_exists('dash_sub_menu') ) {
	/**
	 * Through this function, sub menu of the respective parent menu is fetched though parent menu id.
	 *
	 * @param int $parent_id
	 * @return void
	 */
	function dashboardChildMenu($parent_id)
	{
		$CI =& get_instance();
		
		$where = array(
			'status' => 1,
			'parent_menu_id' => $parent_id
		);

		$CI->db->order_by('order_by');
		$result = $CI->db->get_where('dashboard_menu', $where);

		if ($result->num_rows() > 0) {
			return $result->result();

		} else {
			return false;
		}
	}
}


if ( !function_exists('generateRandomString') ) {
	/**
	 * This function generates random string of any length. By default, it generates string of length 10. The value of length can be determined by passing the respective value through the param.
	 *
	 * @param integer $length
	 * @return string
	 */
	function generateRandomString($length = 10) {
		return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
	}
}



