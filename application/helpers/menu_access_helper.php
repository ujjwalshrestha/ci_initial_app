<?php 

if( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( !function_exists('viewAccess') ) {
    /**
     * Undocumented function
     *
     * @param [type] $menuId
     * @return void
     */
	function viewAccess($menuId)
	{
        $CI =& get_instance();
        $groupId = session_data('group_id');
        
        $where = array(
			'group_id' => $groupId,
			'menu_id' => $menuId
		);

		$result = $CI->db->get_where('dashboard_menu_access', $where);

		if ($result->num_rows() == 1) {
			return $result->row()->view_access;

		} else {
			return false;
		}
	}
}

if ( !function_exists('addAccess') ) {
    /**
     * Undocumented function
     *
     * @param [type] $menuId
     * @return void
     */
	function addAccess($menuId)
	{
        $CI =& get_instance();
        $groupId = session_data('group_id');
        
        $where = array(
			'group_id' => $groupId,
			'menu_id' => $menuId
		);

		$result = $CI->db->get_where('dashboard_menu_access', $where);

		if ($result->num_rows() == 1) {
			return $result->row()->add_access;

		} else {
			return false;
		}
	}
}

if ( !function_exists('editAccess') ) {
    /**
     * Undocumented function
     *
     * @param [type] $menuId
     * @return void
     */
	function editAccess($menuId)
	{
        $CI =& get_instance();
        $groupId = session_data('group_id');
        
        $where = array(
			'group_id' => $groupId,
			'menu_id' => $menuId
		);

		$result = $CI->db->get_where('dashboard_menu_access', $where);

		if ($result->num_rows() == 1) {
			return $result->row()->edit_access;

		} else {
			return false;
		}
	}
}

if ( !function_exists('deleteAccess') ) {
    /**
     * Undocumented function
     *
     * @param [type] $menuId
     * @return void
     */
	function deleteAccess($menuId)
	{
        $CI =& get_instance();
        $groupId = session_data('group_id');
        
        $where = array(
			'group_id' => $groupId,
			'menu_id' => $menuId
		);

		$result = $CI->db->get_where('dashboard_menu_access', $where);

		if ($result->num_rows() == 1) {
			return $result->row()->delete_access;

		} else {
			return false;
		}
	}
}